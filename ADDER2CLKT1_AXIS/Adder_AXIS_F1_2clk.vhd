library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;
use work.AXIS_F1_2clk_pkg.all;

-- Example of wrapping simple pipelined DSP adder in AXI Stream
entity Adder_AXIS_F1_2clk is 
    generic (AXI_DATA_WIDTH : integer := 64;
			 S_AXI_FIFO_DEPTH : integer := 6;
			 M_AXI_FIFO_DEPTH : integer := 7;
			 ADD_VAL : integer := 69);
    port(-- AXIS Clocks/Resets
		 AXIS_ACLK	   : in  std_logic;
         AXIS_RESETN   : in  std_logic;
		 -- DSP Clocks 
		 DSP_CLK       : in  std_logic;
		 -- Slave Interface
		 S0_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 S0_AXIS_TVALID : in  std_logic;
		 S0_AXIS_TREADY : out std_logic;
		 S0_AXIS_TLAST  : in  std_logic;
		 S0_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
		 -- Master Interface 
		 M0_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 M0_AXIS_TVALID : out std_logic;
		 M0_AXIS_TREADY : in  std_logic;
		 M0_AXIS_TLAST  : out std_logic;
		 M0_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0));
end Adder_AXIS_F1_2clk;

architecture behavior of Adder_AXIS_F1_2clk is
	-- Constant, Signal, and Component Declarations
	-- Constants
    constant AXI_TKEEP_WIDTH : integer := AXI_DATA_WIDTH/8;
    constant MODULE_DELAY : integer := 1; -- Delay of the pipelined module.
	constant FSM_COUNT_WIDTH : integer := 8; -- Counter width for Pipe'd DSP control. Needs to handle the DSPs delay.
	constant Data_width : integer := 32; -- Internal DSP processing width. 
	-- Signals
	-- Slave Signals
	signal S_DATA : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal S_VAL  : std_logic := '0';
	signal S_RDY  : std_logic := '0';
	signal S_LAST : std_logic := '0';
	signal S_KEEP : std_logic_vector(AXI_TKEEP_WIDTH-1 downto 0) := (others => '0');
	-- Master signals 
	signal M_DATA : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal M_VAL  : std_logic := '0';
	signal M_RDY  : std_logic := '0';
	signal M_LAST : std_logic := '0';
	signal M_KEEP : std_logic_vector(AXI_TKEEP_WIDTH-1 downto 0) := (others => '0');
	-- DSP Signals 
	signal DSP_DIN : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal DSP_VAL_IN : std_logic := '0';
	signal DSP_LAST_IN : std_logic := '0';
	signal DSP_RST : std_logic := '0'; 
	--
	signal DSP_DOUT : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal DSP_EN   : std_logic := '0';
	signal DSP_VAL_OUT : std_logic := '0';
	signal DSP_LAST_OUT : std_logic := '0';   
	-- Control Signals 
	signal LOAD_EMPT_EN : std_logic := '0';
	signal PROC_EN : std_logic := '0'; 
	signal S_LOAD_DONE : std_logic := '0';
	signal S_PROC_DONE : std_logic := '0';
	signal M_EMPT_DONE : std_logic := '0';
	signal M_PROC_DONE : std_logic := '0';
	-- USER DSP SIGNALS
	 
	-- DSP Component
	component adder_module_en is
		generic(width_in : integer := 2);
		Port ( clk : in STD_LOGIC;
			   ce : in STD_LOGIC;
			   en : in STD_LOGIC;
			   a_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
			   b_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
			   d_out : out STD_LOGIC_VECTOR (width_in downto 0));
	end component adder_module_en;
    -- End Signals and Components
begin
-- Signal Assignments
	-- Slave Signal Assignments
	S_DATA <= S0_AXIS_TDATA; 
	S_VAL  <= S0_AXIS_TVALID;
	S0_AXIS_TREADY <= S_RDY;
	S_LAST <= S0_AXIS_TLAST; 
	S_KEEP <= S0_AXIS_TKEEP; 
	-- Master Slave Signal Assignments
	M0_AXIS_TDATA  <= M_DATA;
	M0_AXIS_TVALID <= M_VAL;
	M_RDY <= M0_AXIS_TREADY;
	M0_AXIS_TLAST  <= M_LAST;
	M0_AXIS_TKEEP  <= M_KEEP;
	-- DSP Signal Assignments 
   
	
	
	-- Component Instantiations
	-- Slave AXI Stream Buffer
	S_AXIS_F1_inst: S_AXIS_F1_2clk
		generic map(AXI_DATA_WIDTH => AXI_DATA_WIDTH,
					AXI_FIFO_DEPTH => S_AXI_FIFO_DEPTH)
		port map(AXIS_ACLK => AXIS_ACLK,
				 AXIS_RESETN => AXIS_RESETN,
				 S_AXIS_TDATA => S_DATA,
				 S_AXIS_TVALID => S_VAL,
				 S_AXIS_TREADY => S_RDY,
				 S_AXIS_TLAST => S_LAST,
				 S_AXIS_TKEEP => S_KEEP,
				 LOAD_DONE => S_LOAD_DONE,
				 LOAD_EN => LOAD_EMPT_EN,
				 PROC_EN => PROC_EN,
				 DSP_CLK => DSP_CLK,
				 PROC_DONE => S_PROC_DONE,
				 DSP_DATA_OUT => DSP_DIN,
				 DSP_VAL_OUT => DSP_VAL_IN,
				 LAST_OUT => DSP_LAST_IN);
	
	-- RST Synchronizer
	proc_synch_inst: clock_cross_bin
		port map(clk_in => AXIS_ACLK,
				 clk_out => DSP_CLK,
				 data_in => not(AXIS_RESETN),
				 data_out => DSP_RST);	
	
	-- Pipelined DSP FSM Instantiation 
	PIPE_DSP_FSM_inst: PIPE_DSP_FSM
		generic map(module_delay => MODULE_DELAY,
					counter_width => FSM_COUNT_WIDTH)
		port map(clk => DSP_CLK,
				 rst => DSP_RST,
				 ce => '1',
				 en => '1',
				 val_in => DSP_VAL_IN,
				 last_in => DSP_LAST_IN,
				 DSP_en => DSP_EN,
				 val_out => DSP_VAL_OUT,
				 last_out => DSP_LAST_OUT,
				 state_out => open);
				 
	-- INSTANTIATE DSP MODULE HERE:
	-- Enabled Adder module 
	Adder_module_inst: adder_module_en
		generic map(width_in => Data_width)
		port map(clk => DSP_CLK,
				 ce => '1',
				 en => DSP_EN,
				 a_in => DSP_DIN(Data_width-1 downto 0),
				 b_in => std_logic_vector(to_unsigned(ADD_VAL,32)),
				 d_out => DSP_DOUT(Data_width downto 0));
	
	-- Master AXI Stream Buffer	
	M_AXIS_F1_inst: M_AXIS_F1_2clk
		generic map(AXI_DATA_WIDTH => AXI_DATA_WIDTH,
					AXI_FIFO_DEPTH => M_AXI_FIFO_DEPTH)
		port map(AXIS_ACLK => AXIS_ACLK,
				 AXIS_RESETN => AXIS_RESETN,
				 M_AXIS_TDATA => M_DATA,
				 M_AXIS_TVALID => M_VAL,
				 M_AXIS_TREADY => M_RDY,
				 M_AXIS_TLAST => M_LAST,
				 M_AXIS_TKEEP => M_KEEP,
				 EMPTY_DONE => M_EMPT_DONE,
				 EMPTY_EN => LOAD_EMPT_EN,
				 PROC_EN => PROC_EN,
				 DSP_CLK => DSP_CLK,
				 PROC_DONE => M_PROC_DONE,
				 DSP_DATA_IN => DSP_DOUT,
				 DSP_VAL_IN => DSP_VAL_OUT,
				 LAST_IN => DSP_LAST_OUT);
	
	-- Load/Empty and Process control FSM
	AXIS_CYCLE_FSM_inst: AXIS_CYCLE_FSM_2clk
		port map(AXIS_ACLK => AXIS_ACLK,
				 rst => not(AXIS_RESETN),
				 S_LOAD_DONE => S_LOAD_DONE,
				 M_EMPT_DONE => M_EMPT_DONE,
				 LOAD_EMPT_EN => LOAD_EMPT_EN,
				 PROC_EN => PROC_EN,
				 state_out => open,
				 DSP_CLK => DSP_CLK,
				 S_PROC_DONE => S_PROC_DONE,
				 M_PROC_DONE => M_PROC_DONE);
	
end behavior;  
