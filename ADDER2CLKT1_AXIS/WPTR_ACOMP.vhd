-- Read Pointer Comparing File 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.ALL;

entity WPTR_ACOMP is
	generic (ADDR : integer := 10);
	port(wclk     : in  std_logic;
		 wrst_n   : in  std_logic;
		 wen      : in  std_logic;
		 afull_n : in  std_logic;
		 wfull   : out std_logic;
		 wptr     : out std_logic_vector(ADDR-1 downto 0));
end WPTR_ACOMP;

architecture behavior of WPTR_ACOMP is
-- Signal Instantiations
	-- Reg signals
	signal wbin : std_logic_vector(ADDR-1 downto 0) := (others => '0');
	signal wptr_reg : std_logic_vector(ADDR-1 downto 0) := (others => '0');
	signal wfull_reg : std_logic := '0';
	signal wfull2_reg : std_logic := '0';
	
	-- Wire signals 
	signal wgraynext : std_logic_vector(ADDR-1 downto 0);
	signal wbinnext : std_logic_vector(ADDR-1 downto 0);
	signal add_sig : std_logic_vector(ADDR-1 downto 0);
	
begin
	-- Basic assignments
	add_sig(ADDR-1 downto 1) <= (others => '0');
	add_sig(0) <= wen and not(wfull_reg);
	-- Reg assignments 
	wfull <= wfull_reg;
	wptr <= wptr_reg; 
	-- Combinational assignments 
	wbinnext <= wbin + add_sig; 
	wgraynext <= ('0' & (wbinnext(ADDR-1 downto 1)) xor wbinnext);	
	
	-- Main proc to reset and advance rbin,rptr_reg
	main_proc: process(wclk,wrst_n) begin
		if(wrst_n='0') then
			wbin <= (others => '0');
			wptr_reg <= (others => '0');
		elsif wclk'event and wclk='1' then  
			wptr_reg <= wgraynext;
			wbin <= wbinnext;
		end if;
	end process main_proc;
	
	-- Afull Process
	afull_proc: process(wclk,wrst_n,afull_n) begin
		if(wrst_n = '0') then 
			wfull_reg <= '0';
			wfull2_reg <= '0';
		elsif(afull_n = '0') then
			wfull_reg <= '1';
			wfull2_reg <= '1';
		elsif wclk'event and wclk='1' then  
				wfull_reg <= wfull2_reg;
				wfull2_reg <= not(afull_n);
		end if;
	end process afull_proc;
	
end behavior;	