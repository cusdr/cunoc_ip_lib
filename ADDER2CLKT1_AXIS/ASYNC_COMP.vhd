library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.math_real.all;

entity ASYNC_COMP is
    generic (ADDR  : integer := 5);
    port (wptr	   : in  std_logic_vector (ADDR-1 downto 0);
		  rptr	   : in  std_logic_vector (ADDR-1 downto 0);
  		  wrst_n   : in  std_logic;
		  aempty_n : out std_logic;
		  afull_n  : out std_logic);
end ASYNC_COMP;

architecture behavior of ASYNC_COMP is 
-- Signal Instantiation
	-- Reg signals
	signal dir : std_logic := '0';
	-- Wire signals 
	signal dirset_n : std_logic;
	signal dirclr_n : std_logic;
	signal eq_sig : std_logic;
	signal high_sig : std_logic;
begin
	high_sig <= '1';
	dirset_n <= not((wptr(ADDR-1) xor rptr(ADDR-2)) and not(wptr(ADDR-2) xor rptr(ADDR-1)));
	dirclr_n <= not((not(wptr(ADDR-1) xor rptr(ADDR-2)) and (wptr(ADDR-2) xor rptr(ADDR-1))) or not(wrst_n));

	dir_proc: process(dirset_n,dirclr_n) begin 
        if(dirclr_n = '0') then 
            dir <= '0';
        elsif(falling_edge(dirset_n)) then  
            dir <= '1';
        end if;
	end process dir_proc;
	
	eq_sig <= '1' when (wptr=rptr) else '0';
	afull_n <= not(eq_sig and dir);
	aempty_n <= not(eq_sig and not(dir));
	
end behavior;
