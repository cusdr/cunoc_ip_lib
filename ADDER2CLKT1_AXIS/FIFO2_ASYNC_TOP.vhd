-- Top 2-clk FIFO
-- Based on:
-- Simulation and Synthesis Techniques for Asynchronous FIFO Design
-- By:
-- Clifford E. Cummings, Sunburst Design, Inc. cliffc@sunburst-design.com

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity FIFO2_ASYNC_TOP is
    generic (DATA_WIDTH :integer := 64;
			 ADDR_WIDTH :integer := 9);
    port (-- Reading port.
		  Data_out    :out std_logic_vector (DATA_WIDTH-1 downto 0);
		  Empty_out   :out std_logic;
		  ReadEn_in   :in  std_logic;
		  RClk        :in  std_logic;
		  -- Writing port.
		  Data_in     :in  std_logic_vector (DATA_WIDTH-1 downto 0);
		  Full_out    :out std_logic;
		  WriteEn_in  :in  std_logic;
		  WClk        :in  std_logic;
		  -- rst
		  RRst_n	  :in  std_logic;
		  WRst_n	  :in  std_logic);
end FIFO2_ASYNC_TOP;

architecture behavior of FIFO2_ASYNC_TOP is
	-- Component Declarations
	-- Dual Port RAM
	component DP_RAM2 is
		generic (DATA    : integer := DATA_WIDTH;
				 ADDR    : integer := ADDR_WIDTH);
		port (wclk    : in  std_logic;
			  wen     : in  std_logic;
			  waddr   : in  std_logic_vector(ADDR-1 downto 0);
			  wdata   : in  std_logic_vector(DATA-1 downto 0);
			  rclk    : in  std_logic;
			  ren     : in  std_logic;
			  raddr   : in  std_logic_vector(ADDR-1 downto 0);
			  rdata   : out std_logic_vector(DATA-1 downto 0));
	end component DP_RAM2;
	
	-- Async Comparator 
	component ASYNC_COMP is
		generic (ADDR  : integer := ADDR_WIDTH);
		port (wptr	   : in  std_logic_vector (ADDR-1 downto 0);
			  rptr	   : in  std_logic_vector (ADDR-1 downto 0);
			  wrst_n   : in  std_logic;
			  aempty_n : out std_logic;
			  afull_n  : out std_logic);
	end component ASYNC_COMP;
	
	-- Read Pointer Comparator
	component RPTR_ACOMP is
		generic (ADDR : integer := ADDR_WIDTH);
		port(rclk     : in  std_logic;
			 rrst_n   : in  std_logic;
			 ren      : in  std_logic;
			 aempty_n : in  std_logic;
			 rempty   : out std_logic;
			 rptr     : out std_logic_vector(ADDR-1 downto 0));
	end component RPTR_ACOMP;
	
	-- Write Pointer Comparator
	component WPTR_ACOMP is
		generic (ADDR : integer := ADDR_WIDTH);
		port(wclk     : in  std_logic;
			 wrst_n   : in  std_logic;
			 wen      : in  std_logic;
			 afull_n : in  std_logic;
			 wfull   : out std_logic;
			 wptr     : out std_logic_vector(ADDR-1 downto 0));
	end component WPTR_ACOMP;
	
	-- Signal Declarations
	signal wptr_int : std_logic_vector(ADDR_WIDTH-1 downto 0);
	signal rptr_int : std_logic_vector(ADDR_WIDTH-1 downto 0);
	signal aempty_n_int : std_logic;
	signal afull_n_int : std_logic;

begin 
	-- Signal Assignments
	-- Component Instantiation		 
	DP_RAM2_inst: DP_RAM2
		port map(wclk  => WClk,
				 wen   => WriteEn_in,-- need to do that 
				 waddr => wptr_int,
				 wdata => Data_in,
				 rclk  => RClk,
				 ren   => ReadEn_in,
				 raddr => rptr_int,
				 rdata => Data_out);
				 
	ASYNC_COMP_inst: ASYNC_COMP
		port map(wptr     => wptr_int,	  
		         rptr     => rptr_int,	  
		         wrst_n   => WRst_n,
		         aempty_n => aempty_n_int,
		         afull_n  => afull_n_int);
				 
	RPTR_ACOMP_inst: RPTR_ACOMP
		port map(rclk     => RClk,    
		         rrst_n   => RRst_n,
		         ren      => ReadEn_in,     
		         aempty_n => aempty_n_int,
		         rempty   => Empty_out, 
		         rptr     => rptr_int);
				 
	WPTR_ACOMP_inst: WPTR_ACOMP
		port map(wclk    => WClk, 
		         wrst_n  => WRst_n,
		         wen     => WriteEn_in,
		         afull_n => afull_n_int,
		         wfull   => Full_out,
		         wptr    => wptr_int);

end behavior;
				
	
				 
	
	