library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;

entity moving_avg_filt_en is 

    generic (data_width  : integer := 24; -- data width in
             mov_avg_win : integer := 8  -- log2(size of moving average window)
    );

    port(
		clk	     : in   std_logic;
        ce       : in   std_logic;
        rst      : in   std_logic;
		en		 : in   std_logic; 
        Mag_in   : in   std_logic_vector(data_width-1 downto 0);
		Avg_out  : out	std_logic_vector(data_width-1 downto 0)
    );

end moving_avg_filt_en;

architecture behavior of moving_avg_filt_en is
    -- Constants 
    constant accum_size : integer := data_width+mov_avg_win;
    -- Signals
    signal rd_addr : std_logic_vector(mov_avg_win-1 downto 0) := (others => '0');-- I squared
    signal wr_addr : std_logic_vector(mov_avg_win-1 downto 0) := (others => '0');-- Q squared
    signal Mag_reg_in : std_logic_vector(accum_size-1 downto 0) := (others => '0');
    signal accum_reg_z1 : std_logic_vector(accum_size-1 downto 0) := (others => '0');
    signal temp_result_reg : std_logic_vector(accum_size-1 downto 0) := (others => '0');    
    signal data_delay_z1 : std_logic_vector(accum_size-1 downto 0) := (others => '0'); 
    signal data_delay_z2 : std_logic_vector(accum_size-1 downto 0) := (others => '0');
    -- Component Declaration
    -- Simple Counter for Addressing 
    component counter_simp_en is 
    generic (data_width : integer := mov_avg_win;
             step_size  : integer := 1);
        port(
            clk  : in   std_logic;
            ce   : in   std_logic;
            rst  : in   std_logic;
			en   : in   std_logic; 
            dout : out  std_logic_vector(data_width-1 downto 0));
    end component counter_simp_en;
    --
    -- Block RAM for Data Delay
    component Dual_Port_Block_RAM is
    generic (DATA    : integer := data_width;
             ADDR    : integer := mov_avg_win);
    port (
        clk     : in  std_logic;
        ce      : in  std_logic;
        a_wr    : in  std_logic;
        a_addr  : in  std_logic_vector(ADDR-1 downto 0);
        a_din   : in  std_logic_vector(DATA-1 downto 0);
        a_dout  : out std_logic_vector(DATA-1 downto 0);
        b_wr    : in  std_logic;
        b_addr  : in  std_logic_vector(ADDR-1 downto 0);
        b_din   : in  std_logic_vector(DATA-1 downto 0);
        b_dout  : out std_logic_vector(DATA-1 downto 0));
    end component Dual_Port_Block_RAM;
    -- End Signals and Components
begin
-- Component Instantiations
    -- Addressing Counter
    count_addr: counter_simp_en
        port map(clk => clk,
                 ce => ce,
                 rst => rst,
				 en  => en,
                 dout => wr_addr);
                 
    -- Block RAM for data delay
    delay_RAM: Dual_Port_Block_RAM
        port map(clk => clk,
                 ce  => ce,
                 a_wr => '1',  
                 a_addr => wr_addr, 
                 a_din => Mag_in,
                 a_dout => open,
                 b_wr  => '0',   
                 b_addr => rd_addr,
                 b_din  => (others => '0'),
                 b_dout => data_delay_z1(data_width-1 downto 0));
           
-- Processes
    accum_proc: process(clk,rst,en) begin
        if clk'event and clk = '1' then
           if(rst='1') then 
               accum_reg_z1 <= (others => '0');
           elsif((en = '1') and (rst = '0')) then 
               Mag_reg_in(data_width-1 downto 0) <= Mag_in;-- Register the data in
               data_delay_z2 <= data_delay_z1; -- Put the memory data through 1 extra delay to match incomming data delay
               temp_result_reg <= Mag_reg_in-data_delay_z2; -- do the adds and subs
               accum_reg_z1 <= accum_reg_z1+temp_result_reg; -- do the adds and subs
               Avg_out <= accum_reg_z1(accum_size-1 downto accum_size-data_width);-- results                
            end if;
        end if;
    end process accum_proc;
    
-- Signal Declarations
    rd_addr <= wr_addr+ conv_std_logic_vector(1,mov_avg_win); 
end behavior;  