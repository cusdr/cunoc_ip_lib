----------------------------------------------------------------------------------
-- Company: CU Boulder
-- Engineer: Austin Anderson 
-- 
-- Create Date: 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: Zedboard
-- Tool Versions: 
-- Description: Finite state machine to control CFAR detection logic.
-- 
-- Dependencies: TBD
-- 
-- Revision: 1
-- Revision 0.01 - File Created
-- Additional Comments:
-- I'm a bit rusty so be gentle...
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;


entity smoother_rst_fsm_en is
    generic(data_width     : integer := 24; -- data width in 
            smoother_len   : integer := 8);--log2 of smoother window length
    Port (clk, rst, ce:  in  STD_LOGIC;
		  en          :  in  STD_LOGIC;
          data_in     :  in  STD_LOGIC_VECTOR(data_width-1 downto 0); -- minimum time to count as a valid detection
          data_val    :  out STD_LOGIC; -- output valid
          data_out    :  out STD_LOGIC_VECTOR(data_width-1 downto 0);-- data output
          state_out   :  out STD_LOGIC_VECTOR(1 downto 0) -- current state
     );
end smoother_rst_fsm_en;

architecture Behavioral of smoother_rst_fsm_en is
    -- Types
    type statetype is (S0_rst, S1_trans, S2_done);
    -- Signals
    signal state, nextstate: statetype;
    signal count_rst : std_logic := '0';-- don't reset the counter once its started 
    signal count_int : std_logic_vector(smoother_len downto 0); 
    signal count1_check : std_logic_vector(smoother_len downto 0) := (others => '1');
    signal count2_check : std_logic_vector(smoother_len downto 0) := (others => '1');
    signal alt_in : std_logic_vector(data_width-1 downto 0) := (others => '0');
    signal alt_out : std_logic_vector(data_width-1 downto 0) := (others => '1');
    signal reg_in : std_logic_vector(data_width-1 downto 0) := (others => '0');
    signal reg_out : std_logic_vector(data_width-1 downto 0) := (others => '0');
    signal count1_eq : std_logic;
    signal count2_eq : std_logic;
    -- Components 
    
    -- Simple Counter for Addressing 
    component counter_simp_en is 
    generic (data_width : integer := smoother_len+1;
             step_size  : integer := 1);
        port(
            clk  : in   std_logic;
            ce   : in   std_logic;
            rst  : in   std_logic;
			en   : in   std_logic;
            dout : out  std_logic_vector(data_width-1 downto 0));
    end component counter_simp_en;

    -- Moving Average Filter
    component moving_avg_filt_en is
    generic (data_width  : integer := data_width; 
             mov_avg_win : integer := smoother_len);
    port(clk      : in   std_logic;
         ce       : in   std_logic;
         rst      : in   std_logic;
		 en		  : in   std_logic; 
         Mag_in   : in   std_logic_vector(data_width-1 downto 0);
         Avg_out  : out    std_logic_vector(data_width-1 downto 0));
end component moving_avg_filt_en;

begin
   count2_check(smoother_len-1 downto 0) <= (others => '1');
   count2_check(smoother_len) <= '0';
   count1_check <= count2_check+10;
   alt_in <= (others => '0');
   alt_out <= (others => '1');
   --
   count1_check_proc: process(clk,en) begin
       if (clk'event and (clk = '1')) then
		   if (en = '1') then 
			   if (count_int = count1_check) then
				   count1_eq <= '1';
			   else
				   count1_eq <= '0';
			   end if;
			end if; 
       end if;
   end process count1_check_proc;
   --
   count2_check_proc: process(clk,en) begin
       if (clk'event and (clk = '1')) then
			if (en = '1') then 
			   if (count_int = count2_check) then
				   count2_eq <= '1';
			   else
				   count2_eq <= '0';
			   end if;
			end if; 
       end if;
   end process count2_check_proc;
   --data_val <= count_eq;
   -- Counter to check if we're minimum detection
   rst_counter: counter_simp_en
       port map(clk => clk,
                ce  => ce,
                rst => (rst or count_rst),
				en  => en,
                dout => count_int);

   -- Moving Avg Filter
   smoother_inst: moving_avg_filt_en
        port map(clk => clk,
                 ce  => ce,
                 rst => (rst or count_rst),
				 en  => en,
                 Mag_in => reg_in,
                 Avg_out => reg_out);
   
    -- State Reset and Register Logic
    process(clk,rst) begin
        if (rst='1') then 
            state <= S0_rst;
        elsif clk'event and (clk='1') then
			if((en = '1') and (rst = '0')) then 
				state <= nextstate;
			end if; 
        end if;
    end process;
    
    -- Next State Logic
    process(state,count1_eq,count2_eq) begin
        case state is
            when S0_rst => if (count1_eq='1') then 
                                nextstate <= S1_trans;
                                count_rst <= '1';
                            else
                                nextstate <= S0_rst;
                                count_rst <= '0';
                            end if;
            when S1_trans => if (count2_eq='1') then
                                nextstate <= S2_done;
                                count_rst <= '0';
                             else
                                count_rst <= '0';             
                                nextstate <= S1_trans;
                             end if;          
            when S2_done =>  nextstate <= S2_done;
                             count_rst <= '0';             
            when others => nextstate <= S0_rst;
        end case;
    end process;                                        
-- Output Logic and State Process
 process(state,clk) begin
    if clk'event and clk='1' then
     if state = S0_rst then
        data_val <= '0';
        reg_in <= alt_in;
        data_out <= alt_out;
        state_out <= "00";
     elsif state = S1_trans then
        data_val <= '0';
        reg_in <= data_in;
        data_out <= alt_out;
        state_out <= "01";
     elsif state = S2_done then
        data_val <= '1';
        reg_in <= data_in;
        data_out <=reg_out;
        state_out <= "10";
     else
        data_val <= '0';
        reg_in <= data_in;
        data_out <=reg_in;
        state_out <= "00";
     end if;
    end if;
  end process;
 --
end Behavioral;