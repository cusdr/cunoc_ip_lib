----------------------------------------------------------------------------------
-- Company: CU Boulder
-- Engineer: Austin Anderson 
-- 
-- Create Date: 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: Zedboard
-- Tool Versions: 
-- Description: Finite state machine to control CFAR detection logic.
-- 
-- Dependencies: TBD
-- 
-- Revision: 1
-- Revision 0.01 - File Created
-- Additional Comments:
-- I'm a bit rusty so be gentle...
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;
use IEEE.math_real.all;


entity PIPE_DSP_FSM is
	generic(module_delay : integer := 2;
			counter_width: integer := 8);
    Port (clk, rst, ce: in  STD_LOGIC;
          en          : in  STD_LOGIC; -- not sure if we need an enable...
          val_in      : in  STD_LOGIC; -- valid input
          last_in     : in  STD_LOGIC; -- last input
          DSP_en      : out STD_LOGIC; -- trigger is valid
          val_out     : out STD_LOGIC; -- latch detection threshhold
		  last_out    : out STD_LOGIC;
		  state_out   : out STD_LOGIC_VECTOR(1 downto 0)); -- latch detection threshhold);
end PIPE_DSP_FSM;

architecture Behavioral of PIPE_DSP_FSM is
    -- Types
    type statetype is (S0_idle, S1_fill, S2_stream, S3_empt);
    -- Signals
    signal state, nextstate: statetype;
	
	signal last_int : std_logic := '0';
    signal del_count_int : std_logic_vector(counter_width-1 downto 0); 
    signal empt_count_int : std_logic_vector(counter_width-1 downto 0); 
	signal empt_count_en : std_logic := '0';
	signal count_check : std_logic_vector(counter_width-1 downto 0) := std_logic_vector(to_unsigned((module_delay), counter_width)); 
	signal count_rst_val : std_logic := '0';
	
	-- Components 
    
    -- Simple Counter for Addressing 
    component counter_simp_en is 
    generic (data_width : integer := counter_width;
             step_size  : integer := 1);
        port(
            clk  : in   std_logic;
            ce   : in   std_logic;
            rst  : in   std_logic;
			en   : in   std_logic;
            dout : out  std_logic_vector(data_width-1 downto 0));
    end component counter_simp_en;
	
	component shift_reg_en_bool is 
		generic (reg_stages : integer := module_delay);
		port(clk  : in   std_logic;
			 ce   : in   std_logic;
			 en   : in   std_logic;
			 rst  : in   std_logic;
			 din  : in	std_logic;
			 dout : out	std_logic);
	end component shift_reg_en_bool;

begin
   -- Counter to check if we're minimum detection
   del_counter: counter_simp_en
       port map(clk => clk,
                ce  => ce,
                rst => (count_rst_val or rst),
				en  => en and val_in,
                dout => del_count_int);
				
   empt_counter: counter_simp_en
       port map(clk => clk,
                ce  => ce,
                rst => (count_rst_val or rst),
				en  => empt_count_en,
                dout => empt_count_int);				
	zero_check: if(module_delay = 0) generate
		last_int <= last_in;
	end generate zero_check;
	gt_check:   if(module_delay > 0) generate
		shift_reg_tlast_inst: shift_reg_en_bool
			 port map(clk => clk,
					  ce  => '1',
					  en  => '1',
					  rst => rst,
					  din => last_in,
					  dout => last_int);
	end generate gt_check;
    -- State Reset and Register Logic
    process(clk,rst) begin
        if clk'event and (clk='1') then
            if (rst='1') then 
                state <= S0_idle;
            elsif (en = '1') and (rst = '0') then 
                state <= nextstate;
            end if;
        end if;
    end process;
    
    -- Next State Logic
    process(state,val_in,last_in,del_count_int,empt_count_int) begin
        case state is
            when S0_idle => if val_in = '1' then 
                                nextstate <= S1_fill;
								last_out <= '0'; 
								DSP_en <= '1'; 
								val_out <= '0';
								count_rst_val <= '0';
								empt_count_en <= '0'; 
                            else
                                nextstate <= S0_idle;
								last_out <= '0'; 
								DSP_en <= '0'; 
								val_out <= '0';
								count_rst_val <= '1';
								empt_count_en <= '0'; 								
                            end if;
            when S1_fill => if (del_count_int>=count_check) then
								nextstate <= S2_stream;
								last_out <= '0'; 								
								DSP_en <= val_in;
								val_out <= val_in;
								count_rst_val <= '1';
								empt_count_en <= '0'; 
                            else
                                nextstate <= S1_fill;
								last_out <= '0'; 								
								DSP_en <= val_in;
								val_out <= '0'; 
								count_rst_val <= '0';
								empt_count_en <= '0'; 								
                            end if;            
            when S2_stream => if(last_in = '1') then 
								  nextstate <= S3_empt;
								  last_out <= last_int; 								  
								  DSP_en <= val_in; 
								  val_out <= val_in;
								  count_rst_val <= '0';
								  empt_count_en <= '1'; 								  
							  else
                                  nextstate <= S2_stream;
								  last_out <= '0'; 								  								  
								  DSP_en <= val_in;
								  val_out <= val_in;
								  count_rst_val <= '1';
								  empt_count_en <= '0'; 
                              end if;
			when S3_empt => if(empt_count_int>=(count_check+1)) then 
						  	  nextstate <= S0_idle;
							  last_out <= last_int; 								  							  							  
							  DSP_en <= '0';
							  val_out <= '0';	
							  count_rst_val <= '1';
							  empt_count_en <= '0'; 							  
						    else
						  	  nextstate <= S3_empt;
							  last_out <= last_int; 								  							  
							  DSP_en <= '1';
							  val_out <= '1';
							  count_rst_val <= '0';
							  empt_count_en <= '1';							  
						    end if;				  
            when others => nextstate <= S0_idle;
        end case;
    end process;                                        
-- Output Logic and State Process
 process(state,clk) begin
    if clk'event and clk='1' then
     if state = S0_idle then
        state_out <= "00";
     elsif state = S1_fill then
        state_out <= "01";
     elsif state = S2_stream then
        state_out <= "10";
	 elsif state = S3_empt then
        state_out <= "11";	
     else
        state_out <= "00";
     end if;
    end if;
  end process;
 --
end Behavioral;