library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.math_real.all;
    package MOVAVG_AXIS_WRAPPER_pkg is 

	-- Shift register generator for boolean signals. Used for matching delays. 
	component shift_reg_en_bool is 
		generic (reg_stages : integer := 5);
		port(clk  : in   std_logic;
			 ce   : in   std_logic;
			 en   : in   std_logic;
			 rst  : in   std_logic;
			 din  : in	std_logic;
			 dout : out	std_logic);
	end component shift_reg_en_bool;	
	
	-- Simple counter for addressing...and counting
    component counter_simp_en is 
		generic (data_width : integer := 8;
             step_size  : integer := 1);
        port(
            clk  : in   std_logic;
            ce   : in   std_logic;
            rst  : in   std_logic;
			en   : in   std_logic;
            dout : out  std_logic_vector(data_width-1 downto 0));
    end component counter_simp_en;	
	
	-- Standard 1-Clk FIFO for Buffers
	component STD_FIFO is
		Generic (constant DATA_WIDTH  : positive := 64+1;
				 constant FIFO_DEPTH  : positive := 6;
				 MEM_TYPE : string := "distributed");
		Port (CLK	  : in  STD_LOGIC;
			  CE      : in  STD_LOGIC;
			  RST	  : in  STD_LOGIC;
			  WriteEn : in  STD_LOGIC;
			  DataIn  : in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
			  ReadEn  : in  STD_LOGIC;
			  DataOut : out STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
			  Empty	  : out STD_LOGIC;
			  Full	  : out STD_LOGIC);
	end component STD_FIFO;

	-- AXI Stream Slave FIFO Buffer
	component S_AXIS_FIFO is 
		generic (AXI_DATA_WIDTH : integer := 64;
				 AXI_FIFO_DEPTH : integer := 5);-- in log2
		port(-- AXIS Clock Domain
			 -- AXIS_ACLK  : in  std_logic;
			 -- Sysgen Simulation Signals 
			 clk	       : in  std_logic;
			 ce        	   : in  std_logic; 
			 AXIS_RESETN   : in  std_logic;
			 -- Slave Interface
			 S_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
			 S_AXIS_TVALID : in  std_logic;
			 S_AXIS_TREADY : out std_logic;
			 S_AXIS_TLAST  : in  std_logic;
			 S_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
			 -- Buffer Signals
			 LOAD_DONE	   : out std_logic;
			 LOAD_EN       : in  std_logic;
			 -- DSP Clock Domain
			 -- DSP_clk    : in  std_logic;
			 -- Buffer Signals
			 PROC_DONE     : out std_logic;
			 PROC_EN       : in  std_logic;
			 -- DSP Signals Out
			 DSP_DATA_OUT  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
			 DSP_VAL_OUT   : out std_logic;
			 LAST_OUT      : out std_logic);
	end component S_AXIS_FIFO;	
	
	-- AXI Stream Master FIFO Buffer
	component M_AXIS_FIFO is 
		generic (AXI_DATA_WIDTH : integer := 64;
				 AXI_FIFO_DEPTH : integer := 6);-- in log2
		port(-- AXIS Clock Domain
			 -- AXIS_ACLK  : in  std_logic;
			 -- Sysgen Simulation Signals 
			 clk	       : in  std_logic;
			 ce        	   : in  std_logic; 
			 AXIS_RESETN   : in  std_logic;
			 -- Master Interface 
			 M_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
			 M_AXIS_TVALID : out std_logic;
			 M_AXIS_TREADY : in  std_logic;
			 M_AXIS_TLAST  : out std_logic;
			 M_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
			 -- Buffer Signals
			 EMPTY_DONE	   : out std_logic;
			 EMPTY_EN       : in  std_logic;
			 -- DSP Clock Domain
			 -- DSP_clk    : in  std_logic;
			 -- Buffer Signals
			 PROC_DONE     : out std_logic;
			 PROC_EN       : in  std_logic;
			 -- DSP Signals In
			 DSP_DATA_IN   : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
			 DSP_VAL_IN    : in  std_logic;
			 LAST_IN       : in  std_logic);
	end component M_AXIS_FIFO;
	
	-- FSM For Controlling AXI Stream Transfers 
	component AXIS_CYCLE_FSM is
		Port (clk, rst, ce  : in  STD_LOGIC;
			  S_LOAD_DONE   : in  STD_LOGIC; -- Slave Buffer Full
			  S_PROC_DONE   : in  STD_LOGIC; -- Slave Buffer Loaded 
			  M_PROC_DONE   : in  STD_LOGIC; -- Master Buffer Loaded
			  M_EMPT_DONE   : in  STD_LOGIC; -- Master Buffer Empty
			  LOAD_EMPT_EN  : out STD_LOGIC; -- Enable Load/Empty Processes 
			  PROC_EN       : out STD_LOGIC; -- Enable Processing Processes  
			  state_out     : out STD_LOGIC_VECTOR(1 downto 0)); -- State Out
	end component AXIS_CYCLE_FSM;
	
	-- FSM for handling valid, enables, and last for a pipelined DSP
	component PIPE_DSP_FSM is
		generic(module_delay : integer := 2;
				counter_width: integer := 8);
		Port (clk, rst, ce: in  STD_LOGIC;
			  en          : in  STD_LOGIC; -- not sure if we need an enable...
			  val_in      : in  STD_LOGIC; -- valid input
			  last_in     : in  STD_LOGIC; -- last input
			  DSP_en      : out STD_LOGIC; -- trigger is valid
			  val_out     : out STD_LOGIC; -- latch detection threshhold
			  last_out    : out STD_LOGIC;
			  state_out   : out STD_LOGIC_VECTOR(1 downto 0)); -- latch detection threshhold);
	end component PIPE_DSP_FSM;
	

end MOVAVG_AXIS_WRAPPER_pkg;	