library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;

entity non_coh_int_en is 

    generic (data_width  : integer := 24; -- data width in
             nc_stages : integer := 2  -- log2 of number of nci stages
    );

    port(
		clk	      : in   std_logic;
        ce        : in   std_logic;
		en        : in   std_logic; 
        rst       : in   std_logic;
        data_in   : in   std_logic_vector(data_width-1 downto 0);
		data_out  : out	 std_logic_vector(data_width+nc_stages-1 downto 0)
    );

end non_coh_int_en;

architecture behavior of non_coh_int_en is
    -- Constants 
    constant nci_len : integer := 2**nc_stages;
    -- Signals
    signal shift_in_reg : std_logic_vector(data_width*nci_len-1 downto 0);
    -- Component Declaration
    
    -- Shift in data
    component shift_in_gen is 
        generic(width_in : integer := data_width;
                sequence_length : integer := nci_len);
        Port (  clk     : in STD_LOGIC;
                ce      : in STD_LOGIC;
                en      : in STD_LOGIC;
                rst     : in STD_LOGIC;
                data_in : in std_logic_vector(width_in-1 downto 0);
                data_out: inout std_logic_vector(width_in*sequence_length-1 downto 0));
    end component shift_in_gen;
    
    -- Adder tree
    component adder_tree_en is
        generic(addr_width_in : integer := data_width;
                ports_in      : integer := nci_len);
        Port ( clk      : in STD_LOGIC;
               ce       : in STD_LOGIC;
			   en       : in STD_LOGIC;
               data_in  : in STD_LOGIC_VECTOR (addr_width_in*ports_in-1 downto 0);
               data_out : out STD_LOGIC_VECTOR ((addr_width_in+integer(ceil(log2(real(ports_in)))))-1 downto 0));
    end component adder_tree_en;
    
    -- End Constants Signals and Components
begin
-- Component Instantiations
    -- Shift reg instantiation
    shift_reg_inst: shift_in_gen 
        port map(clk => clk,
                 ce  => ce,
                 en  => en,
                 rst => rst,
                 data_in => data_in,
                 data_out => shift_in_reg);
                 
    -- Adder tree instantiation     
    add_tree: adder_tree_en
        port map(clk => clk,
                 ce  => ce,
				 en  => en,
                 data_in => shift_in_reg,
                 data_out => data_out);
end behavior;               