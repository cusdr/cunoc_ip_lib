----------------------------------------------------------------------------------
-- Company: CU Boulder
-- Engineer: Austin Anderson 
-- 
-- Create Date: 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: Zedboard
-- Tool Versions: 
-- Description: Finite state machine to control CFAR detection logic.
-- 
-- Dependencies: TBD
-- 
-- Revision: 1
-- Revision 0.01 - File Created
-- Additional Comments:
-- I'm a bit rusty so be gentle...
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;
use IEEE.math_real.all;

-- Currently for one clock, will upgrade if it works for 2 clocks 
-- 2 CLOCK UPGRADES:
--	- Add synchronizers for DSP signals in and out  
--	- Run the whole thing on the AXIS clock 
entity AXIS_CYCLE_FSM is
    Port (clk, rst, ce  : in  STD_LOGIC;
          S_LOAD_DONE   : in  STD_LOGIC; -- Slave Buffer Full
          S_PROC_DONE   : in  STD_LOGIC; -- Slave Buffer Loaded 
		  M_PROC_DONE   : in  STD_LOGIC; -- Master Buffer Loaded
          M_EMPT_DONE   : in  STD_LOGIC; -- Master Buffer Empty
          LOAD_EMPT_EN  : out STD_LOGIC; -- Enable Load/Empty Processes 
          PROC_EN       : out STD_LOGIC; -- Enable Processing Processes  
		  state_out     : out STD_LOGIC_VECTOR(1 downto 0)); -- State Out
end AXIS_CYCLE_FSM;

architecture Behavioral of AXIS_CYCLE_FSM is
	-- Constants
    -- Types
    type statetype is (S0_load_empt, S1_proc);
    -- Signals
    signal state, nextstate: statetype;
	-- Components 
begin
	-- Would run FSM on AXIS clock and synchronize DSP signals 
    -- State Reset and Register Logic
    process(clk,rst) begin
        if clk'event and (clk='1') then
            if (rst='1') then 
                state <= S0_load_empt;-- set this back to s0...
            else
                state <= nextstate;
            end if;
        end if;
    end process;
    
    -- Next State Logic
    process(state,S_LOAD_DONE,S_PROC_DONE,M_PROC_DONE,M_EMPT_DONE) begin
        case state is
            when S0_load_empt => if ((S_LOAD_DONE = '1') and (M_EMPT_DONE = '1')) then 
                                nextstate <= S1_proc;
                            else
                                nextstate <= S0_load_empt; 								
                            end if;
            when S1_proc => if ((S_PROC_DONE = '1') and (M_PROC_DONE = '1')) then
								nextstate <= S0_load_empt;
							else
                                nextstate <= S1_proc; 								
                            end if;            		  
            when others => nextstate <= S0_load_empt;
        end case;
    end process;                                        
-- Output Logic and State Process
-- Need two Clock Procs for this, but they're well differentiated 
 process(state,clk) begin
    if clk'event and clk='1' then
     if state = S0_load_empt then
		LOAD_EMPT_EN <= '1';
	    PROC_EN <= '0';      
        state_out <= "00";
     elsif state = S1_proc then
		LOAD_EMPT_EN <= '0';
	    PROC_EN <= '1';      
        state_out <= "01";
     else
		LOAD_EMPT_EN <= '0';
	    PROC_EN <= '0';      
        state_out <= "00";
     end if;
    end if;
  end process;
 --
end Behavioral;