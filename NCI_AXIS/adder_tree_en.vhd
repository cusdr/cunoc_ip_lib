----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/04/2015 01:31:43 PM
-- Design Name: 
-- Module Name: adder_tree_en - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- Impliments an adder tree of power 2 number of ports and arbitrary data width.
-- Minimum number of ports is 2. Results in log2(n) latency.
-- Adders are all registered and grow in size by +1 bit per stage. 
-- 
-- Dependencies: 
-- adder_module.vhd
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.math_real.all;

entity adder_tree_en is
    generic(addr_width_in : integer := 2;
            ports_in      : integer := 8);
    Port ( clk      : in STD_LOGIC;
           ce       : in STD_LOGIC;
		   en       : in STD_LOGIC;
           data_in  : in STD_LOGIC_VECTOR (addr_width_in*ports_in-1 downto 0);
           data_out : out STD_LOGIC_VECTOR ((addr_width_in+integer(ceil(log2(real(ports_in)))))-1 downto 0));
end adder_tree_en;

architecture Behavioral of adder_tree_en is
    -- Constant Declarations
    constant width_in : integer := addr_width_in*ports_in;
    constant width_out : integer := addr_width_in+integer(ceil(log2(real(ports_in))));
    constant pe_ports : integer :=ports_in/2;
    constant stages : integer := integer(ceil(log2(real(ports_in))));
    constant array_width : integer := pe_ports*width_out;
    -- Signal Declarations
    
    -- Array Declarations
    type t_arr is array (stages-1 downto 0) of std_logic_vector(array_width-1 downto 0);
    signal pipe_array : t_arr;   
    -- Component Declarations
       
    -- Adder Module
    component adder_module_en is
        generic(width_in : integer := 2);
        port ( clk : in STD_LOGIC;
               ce : in STD_LOGIC;
			   en : in STD_LOGIC;
               a_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
               b_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
               d_out : out STD_LOGIC_VECTOR (width_in downto 0));
    end component adder_module_en;
  
begin

    gen_adder_stages: for I in 0 to stages-1 generate -- Generate Stages
        gen_adder_fan: for J in 0 to integer(2**(stages-1-I))-1 generate -- Generate Fanouts
            first_stage: if I=0 generate -- Source the data in and push data to array for first iteration       
                adderx: adder_module_en 
                    generic map(width_in => addr_width_in+I)
                    port map(clk => clk,
                             ce  => ce,
							 en  => en,
                             a_in => data_in((width_in-J*(2*addr_width_in)-1) downto (width_in-J*(2*addr_width_in)-addr_width_in)),
                             b_in => data_in((width_in-J*(2*addr_width_in)-(1+addr_width_in)) downto (width_in-J*(2*addr_width_in)-2*addr_width_in)),
                             d_out => pipe_array(I)(array_width-width_out*J-1 downto array_width-width_out*J-(addr_width_in+I+1)));
            end generate first_stage;
            --
            next_stage: if I>0 generate -- Build the tree for the remaining iterations
                adderx: adder_module_en 
                    generic map(width_in => addr_width_in+I)
                    port map(clk => clk,
                             ce  => ce,
							 en  => en,
                             a_in => pipe_array(I-1)(array_width-width_out*(J*2+0)-1 downto array_width-width_out*(J*2+0)-(addr_width_in+I)),
                             b_in => pipe_array(I-1)(array_width-width_out*(J*2+1)-1 downto array_width-width_out*(J*2+1)-(addr_width_in+I)),
                             d_out => pipe_array(I)(array_width-width_out*J-1 downto array_width-width_out*J-(addr_width_in+I+1)));
            end generate next_stage;            
        end generate gen_adder_fan;
    end generate gen_adder_stages;
            
    data_out <= pipe_array(stages-1)(array_width-1 downto array_width-width_out);

end Behavioral;
