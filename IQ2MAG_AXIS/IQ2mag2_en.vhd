library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_arith.all;

entity IQ2mag2_en is 

    generic (data_width : integer := 12
    );

    port(
		clk	     : in   std_logic;
        ce       : in   std_logic;
		en       : in   std_logic;
        I_in     : in   std_logic_vector(data_width-1 downto 0);
		Q_in     : in   std_logic_vector(data_width-1 downto 0);
		Mag2_out : out	std_logic_vector(2*data_width-1 downto 0)
    );

end IQ2mag2_en;

architecture behavior of IQ2mag2_en is
    -- Signals
    signal I_2 : std_logic_vector(2*data_width-1 downto 0) := (others => '0');-- I squared
    signal Q_2 : std_logic_vector(2*data_width-1 downto 0) := (others => '0');-- Q squared
	signal Mag2_out_reg :  std_logic_vector(2*data_width-1 downto 0) := (others => '0');-- Mag Squared Out Reg
    -- Component Declaration
    -- Our multiplier
    component pipe_mult_en is 
    generic (size  : integer := data_width; 
             level : integer := 2);
        port(a   : in  std_logic_vector (size-1 downto 0) ;
             b   : in  std_logic_vector (size-1 downto 0) ;
             en  : in  std_logic;
             clk : in  std_logic;
             pdt : out std_logic_vector (2*size-1 downto 0));
    end component pipe_mult_en ;
    -- End Signals and Components
begin
	Mag2_out <= Mag2_out_reg;
-- Component Instantiations
    I_mult: pipe_mult_en
        port map(a => I_in,
                 b => I_in,
				 en  => en,                 
                 clk => clk,
                 pdt => I_2);

    Q_mult: pipe_mult_en
        port map(a => Q_in,
                 b => Q_in,
                 en  => en,
                 clk => clk,
                 pdt => Q_2);
-- Processes
    adder_proc: process(clk,en) begin
        if clk'event and clk = '1' then
			if (en = '1') then 
				Mag2_out_reg <= I_2+Q_2;
			end if; 
        end if;
    end process adder_proc;
end behavior;  
    
    