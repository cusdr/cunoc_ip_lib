-- VHDL example
library ieee ;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

entity pipe_mult_en is

-- generic size is the width of multiplier/multiplicand;
-- generic level is the intended number of stages of the
-- pipelined multiplier;
-- generic level is typically the smallest integer greater
-- than or equal to base 2 logarithm of size, as returned by
-- function log, which you define.
	generic (size : integer := 16; 
	         level : integer := 3
	         );

	port (
		a : in std_logic_vector (size-1 downto 0) ;
		b : in std_logic_vector (size-1 downto 0) ;
		en : in std_logic;
		clk : in std_logic;
		pdt : out std_logic_vector (2*size-1 downto 0));
end pipe_mult_en ;

architecture RTL of pipe_mult_en is
	type levels_of_registers is array (level-1 downto 0) of
	signed (2*size-1 downto 0);
	signal a_int : signed (size-1 downto 0):= (others=>'0');
	signal b_int : signed (size-1 downto 0):= (others=>'0');
	signal pdt_int : levels_of_registers := ((others=> (others=>'0')));

begin
	pdt <= std_logic_vector (pdt_int (level-1));

	process(clk,en) begin
		if clk'event and clk = '1' then
			if (en = '1') then 
				-- multiplier operand inputs are registered
				a_int <= signed (a);
				b_int <= signed (b);
				-- 'level' levels of registers to be inferred at the
				-- output of the multiplier
				pdt_int(0) <= a_int * b_int;
				for i in 1 to level-1 loop
					pdt_int (i) <= pdt_int (i-1);
				end loop;
			end if; 
		end if;
	end process;
end RTL;