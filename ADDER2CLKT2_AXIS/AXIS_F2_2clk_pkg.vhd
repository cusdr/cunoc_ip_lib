library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.math_real.all;

    package AXIS_F2_2clk_pkg is 
	
	-- Synchronizer for binary signals crossing a clock domain
	component clock_cross_bin is
		Port ( clk_in : in STD_LOGIC;
			   clk_out : in STD_LOGIC;
			   data_in : in STD_LOGIC;
			   data_out : out STD_LOGIC);
	end component clock_cross_bin;
	
	-- Shift register generator for boolean signals. Used for matching delays. 
	component shift_reg_en_bool is 
		generic (reg_stages : integer := 5);
		port(clk  : in   std_logic;
			 ce   : in   std_logic;
			 en   : in   std_logic;
			 rst  : in   std_logic;
			 din  : in	std_logic;
			 dout : out	std_logic);
	end component shift_reg_en_bool;	
	
	-- Simple counter for addressing...and counting
    component counter_simp_en is 
		generic (data_width : integer := 8;
             step_size  : integer := 1);
        port(
            clk  : in   std_logic;
            ce   : in   std_logic;
            rst  : in   std_logic;
			en   : in   std_logic;
            dout : out  std_logic_vector(data_width-1 downto 0));
    end component counter_simp_en;	
	
	-- 2 clk FIFO
	component FIFO_ASYNC_TOP is
	    generic (DATA_WIDTH :integer := 64;
				 ADDR_WIDTH :integer := 8);
	    port (-- Reading port.
			  Data_out    :out std_logic_vector (DATA_WIDTH-1 downto 0);
			  Empty_out   :out std_logic;
			  ReadEn_in   :in  std_logic;
			  RClk        :in  std_logic;
			  -- Writing port.
			  Data_in     :in  std_logic_vector (DATA_WIDTH-1 downto 0);
			  Full_out    :out std_logic;
			  WriteEn_in  :in  std_logic;
			  WClk        :in  std_logic;
			  -- rst
			  RRst_n	  :in  std_logic;
			  WRst_n	  :in  std_logic);
	end component FIFO_ASYNC_TOP;
	
	component S_AXIS_F2_2clk is 
		generic (AXI_DATA_WIDTH : integer := 64;
				 AXI_FIFO_DEPTH : integer := 5);-- in log2
		port(-- Sysgen Simulation Signals 
			 -- clk, ce    : in  std_logic; 
			 -- AXIS Clock Domain
			 AXIS_ACLK	   : in  std_logic;
			 AXIS_RESETN   : in  std_logic;
			 -- Slave Interface
			 S_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
			 S_AXIS_TVALID : in  std_logic;
			 S_AXIS_TREADY : out std_logic;
			 S_AXIS_TLAST  : in  std_logic;
			 S_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
			 -- Buffer Signals
			 LOAD_DONE	   : out std_logic;
			 LOAD_EN       : in  std_logic;
			 PROC_EN       : in  std_logic; -- For DSP control, but on AXIS clock domain
			 -- DSP Clock Domain
			 DSP_CLK    : in  std_logic;
			 -- Buffer Signals
			 PROC_DONE     : out std_logic;
			 -- DSP Signals Out
			 DSP_DATA_OUT  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
			 DSP_VAL_OUT   : out std_logic;
			 LAST_OUT      : out std_logic);
	end component S_AXIS_FIFO_2clk;
	
	-- AXI Stream Master FIFO Buffer
	component M_AXIS_F2_2clk is 
		generic (AXI_DATA_WIDTH : integer := 64;
				 AXI_FIFO_DEPTH : integer := 6);-- in log2
		port(-- Sysgen Simulation Signals 
			 -- clk, ce    : in  std_logic; 
			 -- AXIS Clock Domain
			 AXIS_ACLK	   : in  std_logic;
			 AXIS_RESETN   : in  std_logic;
			 -- Master Interface 
			 M_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
			 M_AXIS_TVALID : out std_logic;
			 M_AXIS_TREADY : in  std_logic;
			 M_AXIS_TLAST  : out std_logic;
			 M_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
			 -- Buffer Signals
			 EMPTY_DONE	   : out std_logic;
			 EMPTY_EN       : in  std_logic;
			 PROC_EN       : in  std_logic;  -- For DSP control, but on AXIS clock domain
			 -- DSP Clock Domain
			 DSP_CLK    : in  std_logic;
			 -- Buffer Signals
			 PROC_DONE     : out std_logic;
			 -- DSP Signals In
			 DSP_DATA_IN   : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
			 DSP_VAL_IN    : in  std_logic;
			 LAST_IN       : in  std_logic);
	end component M_AXIS_FIFO_2clk;
	
	-- FSM For Controlling AXI Stream Transfers 
	component AXIS_CYCLE_FSM_2clk is
		Port (-- Sysgen Simulation Signals 
			  --clk, rst, ce  : in  STD_LOGIC;
			  -- AXIS Clock Domain 
			  AXIS_ACLK	    : in  std_logic; -- AXIS Clk 
			  rst           : in  STD_LOGIC; -- rst signal 
			  S_LOAD_DONE   : in  STD_LOGIC; -- Slave Buffer Full
			  M_EMPT_DONE   : in  STD_LOGIC; -- Master Buffer Empty
			  LOAD_EMPT_EN  : out STD_LOGIC; -- Enable Load/Empty Processes 
			  PROC_EN       : out STD_LOGIC; -- Enable Processing Processes  
			  state_out     : out STD_LOGIC_VECTOR(1 downto 0); -- FSM State out
			  -- DSP Clock Domain 
			  DSP_CLK       : in  STD_LOGIC;			  
			  S_PROC_DONE   : in  STD_LOGIC; -- Slave Buffer Loaded 
			  M_PROC_DONE   : in  STD_LOGIC); -- Master Buffer Loaded
	end component AXIS_CYCLE_FSM_2clk;
	
	-- FSM for handling valid, enables, and last for a pipelined DSP
	component PIPE_DSP_FSM is
		generic(module_delay : integer := 2;
				counter_width: integer := 8);
		Port (clk, rst, ce: in  STD_LOGIC;
			  en          : in  STD_LOGIC; -- not sure if we need an enable...
			  val_in      : in  STD_LOGIC; -- valid input
			  last_in     : in  STD_LOGIC; -- last input
			  DSP_en      : out STD_LOGIC; -- trigger is valid
			  val_out     : out STD_LOGIC; -- latch detection threshhold
			  last_out    : out STD_LOGIC;
			  state_out   : out STD_LOGIC_VECTOR(1 downto 0)); -- latch detection threshhold);
	end component PIPE_DSP_FSM;

end AXIS_F2_2clk_pkg;	
