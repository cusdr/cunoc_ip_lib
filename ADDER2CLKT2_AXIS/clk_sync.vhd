-- Clock synchronizing module 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity clk_sync is
generic( data_width : integer := 16);
    Port ( clk      : in  STD_LOGIC;
           rstn     : in  STD_LOGIC;
           data_in  : in  STD_LOGIC_VECTOR(data_width-1 downto 0);
           data_out : out STD_LOGIC_VECTOR(data_width-1 downto 0));
end clk_sync;

architecture Behavioral of clk_sync is
-- Reg signals
    signal data_reg0_out : STD_LOGIC_VECTOR(data_width-1 downto 0) := (others => '0');
    signal data_reg1_out : STD_LOGIC_VECTOR(data_width-1 downto 0) := (others => '0');

-- End signals/components
begin
-- Assignments
    data_out <= data_reg1_out;
-- Procs    
    process_out: process(clk,rstn) begin
        if rstn = '0' then
            data_reg0_out <= (others => '0');
            data_reg1_out <= (others => '0');
        elsif clk'event and clk = '1' then
            data_reg0_out <= data_in;
            data_reg1_out <= data_reg0_out;             
        end if;
    end process;    
--
end Behavioral;