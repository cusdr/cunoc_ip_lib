library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity shift_reg_en_bool is 
    generic (reg_stages : integer := 16);
    port(clk  : in   std_logic;
         ce   : in   std_logic;
		 en   : in   std_logic;
         rst  : in   std_logic;
		 din  : in	std_logic;
		 dout : out	std_logic);
end shift_reg_en_bool;

architecture behavior of shift_reg_en_bool is

    type pipe_arr is array (reg_stages-1 downto 0) of std_logic;
    signal reg_arr : pipe_arr  := (others => '0');

begin
    process (clk) begin
        if clk' event and clk = '1' then
            if rst = '1' then
                reg_arr <= (others => '0');
            elsif ((en = '1') and (rst ='0')) then
                for I in reg_stages-1 downto 0 loop
                    if I=0 then
                        reg_arr(I) <= din; 
                    else
                        reg_arr(I) <= reg_arr(I-1);
                    end if;
                end loop;
		    else 
				reg_arr <= reg_arr;
            end if;
        end if;
    end process; 
    dout <= reg_arr(reg_stages-1);
end behavior;