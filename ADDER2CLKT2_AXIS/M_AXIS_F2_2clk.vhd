library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.math_real.all;

entity M_AXIS_F2_2clk is 
    generic (AXI_DATA_WIDTH : integer := 64;
			 AXI_FIFO_DEPTH : integer := 6);-- in log2
    port(-- Sysgen Simulation Signals 
		 -- clk, ce    : in  std_logic; 
		 -- AXIS Clock Domain
		 AXIS_ACLK	   : in  std_logic;
         AXIS_RESETN   : in  std_logic;
		 -- Master Interface 
		 M_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 M_AXIS_TVALID : out std_logic;
		 M_AXIS_TREADY : in  std_logic;
		 M_AXIS_TLAST  : out std_logic;
		 M_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
		 -- Buffer Signals
		 EMPTY_DONE	   : out std_logic;
		 EMPTY_EN       : in  std_logic;
		 PROC_EN       : in  std_logic;  -- For DSP control, but on AXIS clock domain
		 -- DSP Clock Domain
		 DSP_CLK    : in  std_logic;
		 -- Buffer Signals
		 PROC_DONE     : out std_logic;
		 -- DSP Signals In
		 DSP_DATA_IN   : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 DSP_VAL_IN    : in  std_logic;
		 LAST_IN       : in  std_logic);
end M_AXIS_F2_2clk;

architecture behavior of M_AXIS_F2_2clk is
	-- Constant, Signal, and Component Declarations
    -- Constants

	-- 2 clk FIFO
	component FIFO_ASYNC_TOP is
	    generic (DATA_WIDTH :integer := AXI_DATA_WIDTH+1;
				 ADDR_WIDTH :integer := AXI_FIFO_DEPTH);
	    port (-- Reading port.
			  Data_out    :out std_logic_vector (DATA_WIDTH-1 downto 0);
			  Empty_out   :out std_logic;
			  ReadEn_in   :in  std_logic;
			  RClk        :in  std_logic;
			  -- Writing port.
			  Data_in     :in  std_logic_vector (DATA_WIDTH-1 downto 0);
			  Full_out    :out std_logic;
			  WriteEn_in  :in  std_logic;
			  WClk        :in  std_logic;
			  -- rst
			  RRst_n	  :in  std_logic;
			  WRst_n	  :in  std_logic);
	end component FIFO_ASYNC_TOP;
	
	-- Synchronizer
	component clock_cross_bin is
		Port ( clk_in : in STD_LOGIC;
			   clk_out : in STD_LOGIC;
			   data_in : in STD_LOGIC;
			   data_out : out STD_LOGIC);
	end component clock_cross_bin;
	
	-- Signals
    -- Master Signals
	signal M_RE : std_logic := '0';
	signal M_EMPTY : std_logic := '0';
	signal M_VAL_INT : std_logic := '0';
	signal M_VAL_REG : std_logic := '0';	
	signal LAST_REG : std_logic := '0'; 
	signal DSP_AXIS_RESETN : std_logic :='1';
	-- DSP Signals 
	signal D_WE : std_logic := '0';
	signal D_FULL : std_logic := '0';
	signal FIFO_DOUT : std_logic_vector(AXI_DATA_WIDTH downto 0) := (others => '0');
	signal PROC_EN_DSP : std_logic := '0';
	
begin
	-- Signal Assignments 
	-- MASTER Signal Assignments
	M_AXIS_TKEEP <= (others => '1'); 
	M_VAL_INT <= not(M_EMPTY) and EMPTY_EN; 
	M_RE <= M_AXIS_TREADY and M_VAL_INT;
	M_AXIS_TVALID <= M_VAL_REG;
	M_AXIS_TLAST <= FIFO_DOUT(AXI_DATA_WIDTH) and not(LAST_REG); -- Again, TLAST gets the Rising edge of the fifo last signal
	M_AXIS_TDATA <= FIFO_DOUT(AXI_DATA_WIDTH-1 downto 0); 
	EMPTY_DONE <= M_EMPTY and EMPTY_EN; 
	
	-- DSP Signal Assignments
	D_WE <= not(D_FULL) and DSP_VAL_IN and PROC_EN_DSP;
	PROC_DONE <= (not(DSP_VAL_IN) or D_FULL) and PROC_EN_DSP; -- the D_FULL should never get triggered  
	 	
	fifo_master_2clk_inst: FIFO_ASYNC_TOP
		port map(Data_out => FIFO_DOUT, 
			 Empty_out => M_EMPTY,
			 ReadEn_in => M_RE,
			 RClk => AXIS_ACLK, 
			 Data_in => (LAST_IN & DSP_DATA_IN),
			 Full_out => D_FULL,
			 WriteEn_in => D_WE, 
			 WClk => DSP_CLK, 
			 RRst_n => AXIS_RESETN,
			 WRst_n => DSP_AXIS_RESETN);

	proc_synch_inst: clock_cross_bin
		port map(clk_in => AXIS_ACLK,
				 clk_out => DSP_CLK,
				 data_in => PROC_EN,
				 data_out => PROC_EN_DSP);

	rst_synch_inst: clock_cross_bin
		port map(clk_in => AXIS_ACLK,
			 clk_out => DSP_CLK,
			 data_in => AXIS_RESETN,
			 data_out => DSP_AXIS_RESETN);
				 
	AXIS_Reg_Proc: process (AXIS_ACLK) begin
        if AXIS_ACLK' event and AXIS_ACLK = '1' then						  
		    LAST_REG <= FIFO_DOUT(AXI_DATA_WIDTH);
			M_VAL_REG <= M_VAL_INT; 
		end if;				 
	end process AXIS_Reg_Proc;
	
end behavior;  
	
	
