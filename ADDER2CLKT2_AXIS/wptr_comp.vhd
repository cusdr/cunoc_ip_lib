-- Write Pointer Comparing File 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
 use IEEE.STD_LOGIC_ARITH.ALL;
 
entity wptr_comp is 
    generic (ADDR : integer := 10);
    port(wclk	  : in   std_logic;
         wrst_n   : in   std_logic;
         wen	  : in   std_logic;
		 w_rptr   : in std_logic_vector(ADDR downto 0);
		 waddr    : out std_logic_vector(ADDR-1 downto 0);
		 wptr 	  : out std_logic_vector(ADDR downto 0);
		 wfull    : out	std_logic);
end wptr_comp;

architecture behavior of wptr_comp is
-- Constants 

-- Signals
	-- regs
	signal wfull_reg : std_logic := '0'; 
	signal wptr_reg : std_logic_vector(ADDR downto 0) := (others => '0');
	signal wbin : std_logic_vector(ADDR downto 0) := (others => '0');

	-- wires
	signal wfull_val : std_logic;	
	signal wgraynext : std_logic_vector(ADDR downto 0);
	signal wbinnext : std_logic_vector(ADDR downto 0);
	signal add_sig : std_logic_vector(ADDR downto 0);

begin 

	-- Basic assignments
	add_sig(ADDR downto 1) <= (others => '0');
	add_sig(0) <= wen and not(wfull_reg);
	
	-- reg assign 
	wfull <= wfull_reg;
	wptr <= wptr_reg; 
	waddr <= wbin(ADDR-1 downto 0);
	
	-- Combination Assigns
	wbinnext <= wbin + add_sig;
	wgraynext <= ('0' & (wbinnext(ADDR downto 1)) xor wbinnext);
	wfull_val <= '1' when (wgraynext=(not(w_rptr(ADDR downto ADDR-1)) & w_rptr(ADDR-2 downto 0))) else '0';
	
	-- Main proc to reset and advance rbin,rptr_reg
	main_proc: process(wclk,w_rptr,wgraynext,wen,wfull_val,wrst_n) begin
		if(wrst_n='0') then
			wbin <= (others => '0');
			wptr_reg <= (others => '0');
			wfull_reg <= '0';
		elsif wclk'event and wclk='1' then 
			wbin <= wbinnext;
			wptr_reg <= wgraynext;
			wfull_reg <= wfull_val;
		end if;
	end process main_proc;
end behavior;
