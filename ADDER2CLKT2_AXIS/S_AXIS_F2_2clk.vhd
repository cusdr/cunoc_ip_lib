library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.math_real.all;

entity S_AXIS_F2_2clk is 
    generic (AXI_DATA_WIDTH : integer := 64;
			 AXI_FIFO_DEPTH : integer := 5);-- in log2
    port(-- Sysgen Simulation Signals 
		 -- clk, ce    : in  std_logic; 
		 -- AXIS Clock Domain
		 AXIS_ACLK	   : in  std_logic;
         AXIS_RESETN   : in  std_logic;
		 -- Slave Interface
		 S_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 S_AXIS_TVALID : in  std_logic;
		 S_AXIS_TREADY : out std_logic;
		 S_AXIS_TLAST  : in  std_logic;
		 S_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
		 -- Buffer Signals
		 LOAD_DONE	   : out std_logic;
		 LOAD_EN       : in  std_logic;
		 PROC_EN       : in  std_logic; -- For DSP control, but on AXIS clock domain
		 -- DSP Clock Domain
		 DSP_CLK    : in  std_logic;
		 -- Buffer Signals
		 PROC_DONE     : out std_logic;
		 -- DSP Signals Out
		 DSP_DATA_OUT  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 DSP_VAL_OUT   : out std_logic;
		 LAST_OUT      : out std_logic);
end S_AXIS_F2_2clk;

architecture behavior of S_AXIS_F2_2clk is
	-- Constant, Signal, and Component Declarations
    -- Constants
	
	-- 2 clk FIFO
	component FIFO_ASYNC_TOP is
	    generic (DATA_WIDTH :integer := AXI_DATA_WIDTH+1;
				 ADDR_WIDTH :integer := AXI_FIFO_DEPTH);
	    port (-- Reading port.
			  Data_out    :out std_logic_vector (DATA_WIDTH-1 downto 0);
			  Empty_out   :out std_logic;
			  ReadEn_in   :in  std_logic;
			  RClk        :in  std_logic;
			  -- Writing port.
			  Data_in     :in  std_logic_vector (DATA_WIDTH-1 downto 0);
			  Full_out    :out std_logic;
			  WriteEn_in  :in  std_logic;
			  WClk        :in  std_logic;
			  -- rst
			  RRst_n	  :in  std_logic;
			  WRst_n	  :in  std_logic);
	end component FIFO_ASYNC_TOP;
	
	-- Synchronizer
	component clock_cross_bin is
		Port ( clk_in : in STD_LOGIC;
			   clk_out : in STD_LOGIC;
			   data_in : in STD_LOGIC;
			   data_out : out STD_LOGIC);
	end component clock_cross_bin;

	-- Signals
    -- Slave Signals
	signal S_WE : std_logic := '0';
	signal S_FULL : std_logic := '0';
	signal S_RDY_INT : std_logic := '0';
	signal LAST_DET  : std_logic := '0'; 
	signal PROC_EN_DEL : std_logic := '0'; 
	signal DSP_AXIS_RESETN : std_logic :='1';
	-- DSP Signals 
	signal D_RE : std_logic := '0';
	signal D_RE_REG : std_logic := '0';
	signal D_EMPTY : std_logic := '0';
	signal FIFO_DOUT : std_logic_vector(AXI_DATA_WIDTH downto 0) := (others => '0');
	signal LAST_REG : std_logic := '0'; 
	signal PROC_EN_DSP : std_logic := '0';
	
	
begin
	-- Signal Assignments 
	-- Slave Signal Assignments
	S_RDY_INT <= not(S_FULL) and LOAD_EN;
	S_AXIS_TREADY <= S_RDY_INT;
	S_WE <= S_AXIS_TVALID and S_RDY_INT and LOAD_EN;
	LOAD_DONE <= (S_FULL or LAST_DET) and LOAD_EN;  -- and LOAD_EN?
	
	-- DSP Signal Assignments
	DSP_DATA_OUT <= FIFO_DOUT(AXI_DATA_WIDTH-1 downto 0);
	DSP_VAL_OUT <= D_RE_REG; -- May or may not need a reg..
	LAST_OUT <= FIFO_DOUT(AXI_DATA_WIDTH) and not(LAST_REG);-- Last out gets rising edge of last through the buffer	
	D_RE <= not(D_EMPTY) and PROC_EN_DSP; -- Now only read out when we're in the proc state
	PROC_DONE <= D_EMPTY and PROC_EN_DSP; -- Don't think we need a lot more complicated here. Maybe an and PROC_EN? Maybe not...

	fifo_slave_2clk_inst: FIFO_ASYNC_TOP
		port map(Data_out => FIFO_DOUT,
			 Empty_out => D_EMPTY,
			 ReadEn_in => D_RE,
			 RClk => DSP_CLK,  
			 Data_in => (S_AXIS_TLAST & S_AXIS_TDATA), 
			 Full_out => S_FULL,
			 WriteEn_in => S_WE, 
			 WClk => AXIS_ACLK,
			 RRst_n => DSP_AXIS_RESETN,
			 WRst_n => AXIS_RESETN);

	rst_synch_inst: clock_cross_bin
		port map(clk_in => AXIS_ACLK,
			 clk_out => DSP_CLK,
			 data_in => AXIS_RESETN,
			 data_out => DSP_AXIS_RESETN);
				 
	proc_synch_inst: clock_cross_bin
		port map(clk_in => AXIS_ACLK,
			 clk_out => DSP_CLK,
			 data_in => PROC_EN,
			 data_out => PROC_EN_DSP);
				 
    proc_del_inst: clock_cross_bin
                 port map(clk_in => AXIS_ACLK,
                          clk_out => AXIS_ACLK,
                          data_in => PROC_EN,
                          data_out => PROC_EN_DEL);			 
				 				 
	AXIS_Reg_Proc: process (AXIS_ACLK) begin
        if AXIS_ACLK' event and AXIS_ACLK = '1' then						  
			if((S_AXIS_TLAST = '1') and (LOAD_EN = '1') and (S_FULL = '0')) then 
				LAST_DET <= '1';
			end if; 
			if((LAST_DET = '1') and (PROC_EN_DEL = '1')) then 
				LAST_DET <= '0';
			end if; 
		end if;				 
	end process AXIS_Reg_Proc;
	
	DSP_Reg_Proc: process (DSP_CLK) begin
        if DSP_CLK' event and DSP_CLK = '1' then						  
		    LAST_REG <= FIFO_DOUT(AXI_DATA_WIDTH);
			D_RE_REG <= D_RE; 
		end if;				 
	end process DSP_Reg_Proc;

end behavior;  
	
	
