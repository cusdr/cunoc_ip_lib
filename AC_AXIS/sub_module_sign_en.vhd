----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/04/2015 01:31:43 PM
-- Design Name: 
-- Module Name: adder_tree - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sub_module_sign_en is
    generic(width_in : integer := 2);
    Port ( clk : in STD_LOGIC;
           ce : in STD_LOGIC;
		   en : in STD_LOGIC;		   
           a_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
           b_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
           d_out : out STD_LOGIC_VECTOR (width_in downto 0));
end sub_module_sign_en;

architecture Behavioral of sub_module_sign_en is
begin   
    process(clk) begin
        if clk'event and clk='1' then
			if(en = '1') then 
				d_out <=  (a_in(width_in-1) & a_in) - (b_in(width_in-1) & b_in);
			end if; 
		 end if;
    end process;
end Behavioral;
