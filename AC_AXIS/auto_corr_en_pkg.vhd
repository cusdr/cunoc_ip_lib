library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.math_real.all;
    package auto_corr_en_pkg is 
    -- Components:  
        -- Correlator Processin Element   
	component cor_pe_en is
		Port ( clk : in STD_LOGIC; -- clock
			   ce : in STD_LOGIC; -- clock enable
			   en : in STD_LOGIC;
			   d_in : in STD_LOGIC_VECTOR (1 downto 0); -- data line in
			   c_in : in STD_LOGIC_VECTOR (1 downto 0); -- correlator sequence in
			   d_out : out STD_LOGIC_VECTOR (1 downto 0)); -- result out
	end component cor_pe_en;
        
        -- Adder Module
	component adder_module_en is
		generic(width_in : integer := 2);
		Port ( clk : in STD_LOGIC;
			   ce : in STD_LOGIC;
			   en : in STD_LOGIC;
			   a_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
			   b_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
			   d_out : out STD_LOGIC_VECTOR (width_in downto 0));
	end component adder_module_en;
        
        -- Shift Register
        component shift_in_gen is
            generic(width_in : integer := 1;
                    sequence_length : integer := 8);
            Port (  clk     : in STD_LOGIC;
                    ce      : in STD_LOGIC;
                    en      : in STD_LOGIC;
                    rst     : in STD_LOGIC;
                    data_in : in std_logic_vector(width_in-1 downto 0);
                    data_out: inout std_logic_vector(width_in*sequence_length-1 downto 0));
        end component shift_in_gen;
        
        -- Sign and Reg
        component sign_graber is
            generic(width_in : integer := 16);
            Port ( clk : in STD_LOGIC;
                   ce : in STD_LOGIC;
                   rst : in STD_LOGIC;
                   data_in : in std_logic_vector(width_in-1 downto 0);
                   sign_out : out std_logic_vector(0 downto 0));
        end component sign_graber;
        
        -- Adder Tree
		component adder_tree_en is
			generic(addr_width_in : integer := 2;
					ports_in      : integer := 8);
			Port ( clk      : in STD_LOGIC;
				   ce       : in STD_LOGIC;
				   en       : in STD_LOGIC;
				   data_in  : in STD_LOGIC_VECTOR (addr_width_in*ports_in-1 downto 0);
				   data_out : out STD_LOGIC_VECTOR ((addr_width_in+integer(ceil(log2(real(ports_in)))))-1 downto 0));
		end component adder_tree_en;
        
end auto_corr_en_pkg;