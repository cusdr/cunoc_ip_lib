library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.math_real.all;

entity S_AXIS_FIFO is 
    generic (AXI_DATA_WIDTH : integer := 64;
			 AXI_FIFO_DEPTH : integer := 5);-- in log2
    port(-- AXIS Clock Domain
		 -- AXIS_ACLK  : in  std_logic;
		 -- Sysgen Simulation Signals 
		 clk	       : in  std_logic;
		 ce        	   : in  std_logic; 
         AXIS_RESETN   : in  std_logic;
		 -- Slave Interface
		 S_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 S_AXIS_TVALID : in  std_logic;
		 S_AXIS_TREADY : out std_logic;
		 S_AXIS_TLAST  : in  std_logic;
		 S_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
		 -- Buffer Signals
		 LOAD_DONE	   : out std_logic;
		 LOAD_EN       : in  std_logic;
		 -- DSP Clock Domain
		 -- DSP_clk    : in  std_logic;
		 -- Buffer Signals
		 PROC_DONE     : out std_logic;
		 PROC_EN       : in  std_logic;
		 -- DSP Signals Out
		 DSP_DATA_OUT  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 DSP_VAL_OUT   : out std_logic;
		 LAST_OUT      : out std_logic);
end S_AXIS_FIFO;

architecture behavior of S_AXIS_FIFO is
	-- Constant, Signal, and Component Declarations
    -- Constants
	constant RAM_DEPTH : integer := 2**AXI_FIFO_DEPTH;
	-- Standard FIFO Component
	component STD_FIFO is
		Generic (constant DATA_WIDTH  : positive := AXI_DATA_WIDTH+1;
				 constant FIFO_DEPTH  : positive := RAM_DEPTH;
				 MEM_TYPE : string := "distributed");
		Port (CLK	  : in  STD_LOGIC;
			  CE      : in  STD_LOGIC;
			  RST	  : in  STD_LOGIC;
			  WriteEn : in  STD_LOGIC;
			  DataIn  : in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
			  ReadEn  : in  STD_LOGIC;
			  DataOut : out STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
			  Empty	  : out STD_LOGIC;
			  Full	  : out STD_LOGIC);
	end component STD_FIFO;
	
	-- Signals
    -- Slave Signals
	signal S_WE : std_logic := '0';
	signal S_FULL : std_logic := '0';
	signal S_RDY_INT : std_logic := '0';
	signal LAST_DET  : std_logic := '0'; 
	-- DSP Signals 
	signal D_RE : std_logic := '0';
	signal D_RE_REG : std_logic := '0';
	signal D_EMPTY : std_logic := '0';
	signal FIFO_DOUT : std_logic_vector(AXI_DATA_WIDTH downto 0) := (others => '0');
	signal LAST_REG : std_logic := '0'; 
begin
	-- Signal Assignments 
	-- Slave Signal Assignments
	S_RDY_INT <= not(S_FULL) and LOAD_EN;
	S_AXIS_TREADY <= S_RDY_INT;
	S_WE <= S_AXIS_TVALID and S_RDY_INT;
	LOAD_DONE <= S_FULL or LAST_DET;  -- and LOAD_EN?
	
	-- DSP Signal Assignments
	DSP_DATA_OUT <= FIFO_DOUT(AXI_DATA_WIDTH-1 downto 0);
	DSP_VAL_OUT <= D_RE_REG; -- May or may not need a reg..
	LAST_OUT <= FIFO_DOUT(AXI_DATA_WIDTH) and not(LAST_REG);-- Last out gets rising edge of last through the buffer	
	D_RE <= not(D_EMPTY) and PROC_EN; -- Now only read out when we're in the proc state
	PROC_DONE <= D_EMPTY; -- Don't think we need a lot more complicated here. Maybe an and PROC_EN? Maybe not...

	-- Component Instantiations 
	-- Slave FIFO
	fifo_slave_inst: STD_FIFO
		port map(CLK => clk,
				 CE  => '1',
				 RST => not(AXIS_RESETN),
				 WriteEn => S_WE,
				 DataIn => (S_AXIS_TLAST & S_AXIS_TDATA),
				 ReadEn => D_RE,
				 DataOut => FIFO_DOUT,
				 Empty => D_EMPTY,
				 Full => S_FULL);
				 
	Reg_Proc: process (clk) begin
        if clk' event and clk = '1' then						  
		    LAST_REG <= FIFO_DOUT(AXI_DATA_WIDTH);
			D_RE_REG <= D_RE; 
			if((S_AXIS_TLAST = '1') and (LOAD_EN = '1') and (S_FULL = '0')) then 
				LAST_DET <= '1';
			end if; 
			if((LAST_DET = '1') and (PROC_EN = '1')) then 
				LAST_DET <= '0';
			end if; 
		end if;				 
	end process Reg_Proc;

end behavior;  
	
	