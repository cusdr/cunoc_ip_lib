----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/04/2015 01:31:43 PM
-- Design Name: 
-- Module Name: auto_corr_en - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- auto_corr_en_pkg.vhd
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.math_real.all;
use work.auto_corr_en_pkg.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--result := integer(ceil(log2(real(a))));

entity auto_corr_en is
    generic(width_in : integer := 12;
            sequence_length : integer := 1024);
    Port ( clk       : in STD_LOGIC;
           ce        : in STD_LOGIC;
           rst       : in STD_LOGIC;
           data_in   : in STD_LOGIC_VECTOR (width_in-1 downto 0);
           data_en   : in STD_LOGIC;
           filter_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);     
           filter_en : in STD_LOGIC;
           data_out  : out STD_LOGIC_VECTOR ((2-1+integer(ceil(log2(real(sequence_length/2)))))-1 downto 0));
end auto_corr_en;

architecture Behavioral of auto_corr_en is
    constant addr_width_in : integer := 2;
    constant addr_width_out : integer := addr_width_in+integer(ceil(log2(real(sequence_length/2))));
    signal data_sign : std_logic_vector(0 downto 0);
    signal filter_sign : std_logic_vector(0 downto 0);
    signal data_seq : std_logic_vector(sequence_length-1 downto 0);
    signal filter_seq : std_logic_vector(sequence_length-1 downto 0);
    signal pe_out_pipe : std_logic_vector(sequence_length-1 downto 0);
    signal addr_tree_out :  std_logic_vector(addr_width_out-1 downto 0);
    signal addr_tree_pipe :  std_logic_vector(addr_width_out-1-1 downto 0);    
        
begin
     
     data_sign <= data_in(width_in-1 downto width_in-1);
     filter_sign <= filter_in(width_in-1 downto width_in-1); 
              
     data_shift_in: shift_in_gen
        generic map(width_in => 1,
                    sequence_length => sequence_length)
        port map(clk => clk,
                 ce => ce,
                 en => data_en,
                 rst => rst,
                 data_in => data_sign,
                 data_out => data_seq);
     
     filt_shift_in: shift_in_gen
        generic map(width_in => 1,
                    sequence_length => sequence_length)
        port map(clk => clk,
                 ce => ce,
                 en => filter_en,
                 rst => rst,
                 data_in => filter_sign,
                 data_out => filter_seq);
               
     pe_fan_gen: for I in 0 to sequence_length/2-1 generate
        cor_pe_inst: cor_pe_en
            port map(clk => clk,
                ce  => ce,
				en  => data_en,
                d_in => data_seq(2*I+1 downto 2*I),
                c_in =>  filter_seq(2*I+1 downto 2*I),
                d_out => pe_out_pipe(2*I+1 downto 2*I));
     end generate pe_fan_gen;             
     
     adder_tree_inst : adder_tree_en
        generic map( addr_width_in => addr_width_in,
                     ports_in => sequence_length/2)
        port map( clk => clk,
                  ce => ce,
				  en => data_en,
                  data_in => pe_out_pipe,
                  data_out => addr_tree_out);
                   
     process(addr_tree_out)
     begin 
         if addr_tree_out(addr_width_out-1 downto addr_width_out-1)="1" then
            addr_tree_pipe <= (others => '1');
         else
            addr_tree_pipe <= addr_tree_out(addr_width_out-2 downto 0);
         end if;
     end process;
                         
     data_out <= (not(addr_tree_pipe(addr_width_out-2 downto addr_width_out-2)) & addr_tree_pipe(addr_width_out-3 downto 0)); -- standard, unsigned decimal result
                
end Behavioral;
