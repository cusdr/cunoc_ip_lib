library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;

entity AC_complex_en is 
    generic (IQ_dwid : integer := 16;
			 AC_seq_len : integer := 512);
    port(clk : in  std_logic;
         rst : in  std_logic;
		 ce  : in  std_logic;
		 data_I_in : in std_logic_vector(IQ_dwid-1 downto 0);
		 data_Q_in : in std_logic_vector(IQ_dwid-1 downto 0);
		 data_en   : in  std_logic;
		 filt_I_in : in std_logic_vector(IQ_dwid-1 downto 0);
		 filt_Q_in : in std_logic_vector(IQ_dwid-1 downto 0);
		 filt_en   : in  std_logic;
		 data_I_out : out std_logic_vector((2+integer(ceil(log2(real(AC_seq_len/2)))))-1 downto 0);
		 data_Q_out : out std_logic_vector((2+integer(ceil(log2(real(AC_seq_len/2)))))-1 downto 0));
end AC_complex_en;

architecture behavior of AC_complex_en is
	-- Constant, Signal, and Component Declarations
    -- Constants
	constant ac_width_out : integer := 2-1+integer(ceil(log2(real(AC_seq_len/2))));
    -- Signals
	signal AC_II_Result : std_logic_vector(ac_width_out-1 downto 0) := (others => '0');   
	signal AC_IQ_Result : std_logic_vector(ac_width_out-1 downto 0) := (others => '0');   
	signal AC_QI_Result : std_logic_vector(ac_width_out-1 downto 0) := (others => '0');   
	signal AC_QQ_Result : std_logic_vector(ac_width_out-1 downto 0) := (others => '0');   
	
    -- Component Declaration
	-- Auto Corr
	component auto_corr_en is
		generic(width_in : integer := IQ_dwid;
				sequence_length : integer := AC_seq_len);
		Port ( clk       : in STD_LOGIC;
			   ce        : in STD_LOGIC;
			   rst       : in STD_LOGIC;
			   data_in   : in STD_LOGIC_VECTOR (width_in-1 downto 0);
			   data_en   : in STD_LOGIC;
			   filter_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);     
			   filter_en : in STD_LOGIC;
			   data_out  : out STD_LOGIC_VECTOR ((2-1+integer(ceil(log2(real(sequence_length/2)))))-1 downto 0));
	end component auto_corr;
	
	-- Add Module 
	component adder_module_sign_en is
		generic(width_in : integer := ac_width_out);
		Port ( clk : in STD_LOGIC;
			   ce : in STD_LOGIC;
			   en : in STD_LOGIC;
			   a_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
			   b_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
			   d_out : out STD_LOGIC_VECTOR (width_in downto 0));
	end component adder_module_sign_en;
	
	-- Sub Module  
	component sub_module_sign_en is
		generic(width_in : integer := ac_width_out);
		Port ( clk : in STD_LOGIC;
			   ce : in STD_LOGIC;
			   en : in STD_LOGIC;		   
			   a_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
			   b_in : in STD_LOGIC_VECTOR (width_in-1 downto 0);
			   d_out : out STD_LOGIC_VECTOR (width_in downto 0));
	end component sub_module_sign_en;	
    -- End Signals and Components
begin
-- Component Instantiations
	ac_II_inst : auto_corr_en
		generic map(width_in => IQ_dwid,
					sequence_length => AC_seq_len)
		port map(clk       => clk,
				 ce        => '1',
				 rst       => rst,
				 data_in   => data_I_in,
				 data_en   => data_en,
				 filter_in => filt_I_in,
				 filter_en => filt_en,
				 data_out  => AC_II_Result);

	ac_IQ_inst : auto_corr_en
		generic map(width_in => IQ_dwid,
					sequence_length => AC_seq_len)
		port map(clk       => clk,
				 ce        => '1',
				 rst       => rst,
				 data_in   => data_I_in,
				 data_en   => data_en,
				 filter_in => filt_Q_in,
				 filter_en => filt_en,
				 data_out  => AC_IQ_Result);			 

	ac_QI_inst : auto_corr_en
		generic map(width_in => IQ_dwid,
					sequence_length => AC_seq_len)
		port map(clk       => clk,
				 ce        => '1',
				 rst       => rst,
				 data_in   => data_Q_in,
				 data_en   => data_en,
				 filter_in => filt_I_in,
				 filter_en => filt_en,
				 data_out  => AC_QI_Result);

	ac_QQ_inst : auto_corr_en
		generic map(width_in => IQ_dwid,
					sequence_length => AC_seq_len)
		port map(clk       => clk,
				 ce        => '1',
				 rst       => rst,
				 data_in   => data_Q_in,
				 data_en   => data_en,
				 filter_in => filt_Q_in,
				 filter_en => filt_en,
				 data_out  => AC_QQ_Result);	
				 
	-- Add Sub to Gather Complex terms 
	ac_I_sub_inst : sub_module_sign_en
		generic map(width_in => ac_width_out)
		port map(clk   => clk,
		         ce    => '1',
				 en    => data_en,				 
		         a_in  => AC_II_Result,
		         b_in  => AC_QQ_Result,
		         d_out => data_I_out);
		
	ac_Q_add_inst : adder_module_sign_en
		generic map(width_in => ac_width_out)
		port map(clk   => clk,
		         ce    => '1', 
				 en    => data_en,
		         a_in  => AC_IQ_Result,
		         b_in  => AC_QI_Result,
		         d_out => data_Q_out);				 
							
end behavior;  