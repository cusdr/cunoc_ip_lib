----------------------------------------------------------------------------------
-- Company: CU Boulder
-- Engineer: Austin Anderson
-- 
-- Create Date: 02/04/2015 12:54:06 PM
-- Design Name: Correlator Processing Element 
-- Module Name: cor_pe - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- Smallest processing element of the auto-correlation algorithm developed at CU, see:
-- Blind Synchronization for NC-OFDM - When "Channels" Are Conventions, Not Mandates
-- Dependencies: 
-- None
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cor_pe_en is
    Port ( clk : in STD_LOGIC; -- clock
           ce : in STD_LOGIC; -- clock enable
		   en : in STD_LOGIC;
           d_in : in STD_LOGIC_VECTOR (1 downto 0); -- data line in
           c_in : in STD_LOGIC_VECTOR (1 downto 0); -- correlator sequence in
           d_out : out STD_LOGIC_VECTOR (1 downto 0)); -- result out
end cor_pe_en;

architecture Behavioral of cor_pe_en is
    signal xnor_msb: STD_LOGIC_VECTOR(0 downto 0); -- Reg for msb xnor result
    signal xnor_lsb: STD_LOGIC_VECTOR(0 downto 0); -- Reg for lsb xnor result
begin
    xnor_msb <= d_in(1 downto 1) xnor c_in(1 downto 1);
    xnor_lsb <= d_in(0 downto 0) xnor c_in(0 downto 0);
    process(clk) begin
        if clk'event and clk='1' then
			if(en = '1') then 
				d_out <=  ("0" & xnor_msb) + ("0" & xnor_lsb);
			end if;
		 end if;
    end process;
end Behavioral;
