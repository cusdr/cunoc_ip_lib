----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/06/2015 04:50:31 PM
-- Design Name: 
-- Module Name: shift_in_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shift_in_gen is
    generic(width_in : integer := 1;
            sequence_length : integer := 8);
    Port (  clk     : in STD_LOGIC;
            ce      : in STD_LOGIC;
            en      : in STD_LOGIC;
            rst     : in STD_LOGIC;
            data_in : in std_logic_vector(width_in-1 downto 0);
            data_out: inout std_logic_vector(width_in*sequence_length-1 downto 0));
end shift_in_gen;

architecture Behavioral of shift_in_gen is
    signal default_vector : std_logic_vector(width_in*sequence_length-1 downto 0) := (others => '0');
begin
-- Generate Statements
    reg_chain: for I in 0 to sequence_length-1 generate
        first_reg: if I=0 generate
            process(clk,rst) begin
                if rst='1' then
                    data_out((I*width_in+width_in-1) downto I*width_in) <= default_vector((I*width_in+width_in-1) downto I*width_in);
                elsif rising_edge(clk) then
                    if en = '1' then
                        data_out((I*width_in+width_in-1) downto I*width_in)<= data_in;
                    end if;
                end if;
            end process;
         end generate first_reg;
         rem_reg: if I>0 generate
             process(clk,rst) begin
                if rst='1' then
                    data_out((I*width_in+width_in-1) downto I*width_in) <= default_vector((I*width_in+width_in-1) downto I*width_in);
                elsif rising_edge(clk) then
                    if en = '1' then
                        data_out((I*width_in+width_in-1) downto I*width_in) <= data_out(((I-1)*width_in+width_in-1) downto (I-1)*width_in);
                    end if;
                end if;
            end process;
         end generate rem_reg;
    end generate reg_chain;    
--    
end Behavioral;
