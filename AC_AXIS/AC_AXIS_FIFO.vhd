library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;
use work.AXIS_WRAPPER_pkg.all;

-- Example of wrapping simple pipelined DSP adder in AXI Stream
-- Need to Wrap AC into AC complex of x4
entity AC_AXIS_FIFO is 
    generic (AXI_DATA_WIDTH : integer := 64;
			 S_AXI_FIFO_DEPTH : integer := 5;
			 M_AXI_FIFO_DEPTH : integer := 6;
			 AC_seq_len : integer := 512);
    port(AXIS_ACLK	   : in  std_logic;
         AXIS_RESETN   : in  std_logic;
		 -- Slave Interface for Data
		 S0_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 S0_AXIS_TVALID : in  std_logic;
		 S0_AXIS_TREADY : out std_logic;
		 S0_AXIS_TLAST  : in  std_logic;
		 S0_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
		 -- Slave Interface for Config
		 S1_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 S1_AXIS_TVALID : in  std_logic;
		 S1_AXIS_TREADY : out std_logic;
		 S1_AXIS_TLAST  : in  std_logic;
		 S1_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
		 -- Master Interface 
		 M0_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 M0_AXIS_TVALID : out std_logic;
		 M0_AXIS_TREADY : in  std_logic;
		 M0_AXIS_TLAST  : out std_logic;
		 M0_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0));
end AC_AXIS_FIFO;

architecture behavior of AC_AXIS_FIFO is
	-- Constant, Signal, and Component Declarations
	-- Constants
    constant AXI_TKEEP_WIDTH : integer := AXI_DATA_WIDTH/8;
    constant MODULE_DELAY : integer := integer(ceil(log2(real(AC_seq_len/2))))+1+1+1; -- Ok so its 1 for the shift in, 1 for the PE, log2(seq_len/2) for the n stages of adder tree, and 1 more for the add/sub going out
	constant FSM_COUNT_WIDTH : integer := 8; -- Counter width for Pipe'd DSP control. Needs to handle the DSPs delay.
	constant Data_width : integer := 32; -- Internal DSP processing width.
	constant IQ_dwid : integer := 16; -- IQ data width
	-- USER DSP CONSTANTS
	constant ac_width_out : integer := 2-1+integer(ceil(log2(real(AC_seq_len/2))));
	
	-- Signals
	-- Slave Signals for Data
	signal S0_DATA : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal S0_VAL  : std_logic := '0';
	signal S0_RDY  : std_logic := '0';
	signal S0_LAST : std_logic := '0';
	signal S0_KEEP : std_logic_vector(AXI_TKEEP_WIDTH-1 downto 0) := (others => '0');
	-- Slave Signals for Config
	signal S1_DATA : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal S1_VAL  : std_logic := '0';
	signal S1_RDY  : std_logic := '0';
	signal S1_LAST : std_logic := '0';
	signal S1_KEEP : std_logic_vector(AXI_TKEEP_WIDTH-1 downto 0) := (others => '0');
	-- Master signals 
	signal M_DATA : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal M_VAL  : std_logic := '0';
	signal M_RDY  : std_logic := '0';
	signal M_LAST : std_logic := '0';
	signal M_KEEP : std_logic_vector(AXI_TKEEP_WIDTH-1 downto 0) := (others => '0');
	-- DSP Signals 
	signal DSP_DIN : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal DSP_VAL_IN : std_logic := '0';
	signal DSP_LAST_IN : std_logic := '0';
	--
	signal DSP_DOUT : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal DSP_EN   : std_logic := '0';
	signal DSP_VAL_OUT : std_logic := '0';
	signal DSP_LAST_OUT : std_logic := '0'; 
    -- Config Signals 
	signal CONF_DIN : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal CONF_VAL_IN : std_logic := '0';
	signal CONF_LAST_IN : std_logic := '0';
	signal S1_LOAD_EMPT_EN : std_logic := '0';
	signal S1_PROC_EN : std_logic := '0'; 
	signal S1_LOAD_DONE : std_logic := '0'; 
	signal S1_PROC_DONE : std_logic := '0';
	-- Control Signals 
	signal LOAD_EMPT_EN : std_logic := '0';
	signal PROC_EN : std_logic := '0'; 
	signal S_LOAD_DONE : std_logic := '0'; 
	signal S_PROC_DONE : std_logic := '0'; 
	signal M_EMPT_DONE : std_logic := '0';
	signal M_PROC_DONE : std_logic := '0';
	-- USER DSP SIGNALS
	
	-- DSP Component
	component AC_complex_en is 
		generic (IQ_dwid : integer := IQ_dwid;
				 AC_seq_len : integer := AC_seq_len);
		port(clk : in  std_logic;
			 rst : in  std_logic;
			 ce  : in  std_logic;
			 data_I_in : in std_logic_vector(IQ_dwid-1 downto 0);
			 data_Q_in : in std_logic_vector(IQ_dwid-1 downto 0);
			 data_en   : in  std_logic;
			 filt_I_in : in std_logic_vector(IQ_dwid-1 downto 0);
			 filt_Q_in : in std_logic_vector(IQ_dwid-1 downto 0);
			 filt_en   : in  std_logic;
			 data_I_out : out std_logic_vector((2+integer(ceil(log2(real(AC_seq_len/2)))))-1 downto 0);
			 data_Q_out : out std_logic_vector((2+integer(ceil(log2(real(AC_seq_len/2)))))-1 downto 0));
	end component AC_complex_en;
    -- End Signals and Components
begin
-- Signal Assignments
	-- Slave Signal Assignments
	S0_DATA <= S0_AXIS_TDATA; 
	S0_VAL  <= S0_AXIS_TVALID;
	S0_AXIS_TREADY <= S0_RDY;
	S0_LAST <= S0_AXIS_TLAST; 
	S0_KEEP <= S0_AXIS_TKEEP; 
		-- Slave Signal Assignments
	S1_DATA <= S1_AXIS_TDATA; 
	S1_VAL  <= S1_AXIS_TVALID;
	S1_AXIS_TREADY <= S1_RDY;
	S1_LAST <= S1_AXIS_TLAST; 
	S1_KEEP <= S1_AXIS_TKEEP; 
	-- Master Slave Signal Assignments
	M0_AXIS_TDATA  <= M_DATA;
	M0_AXIS_TVALID <= M_VAL;
	M_RDY <= M0_AXIS_TREADY;
	M0_AXIS_TLAST  <= M_LAST;
	M0_AXIS_TKEEP  <= M_KEEP;
	-- DSP Signal Assignments 
	-- Sign extend IQ output
	DSP_DOUT(Data_width-1 downto (ac_width_out+1+IQ_dwid)) <= (others => DSP_DOUT((ac_width_out+1+IQ_dwid)-1));
	DSP_DOUT(IQ_dwid-1 downto ac_width_out+1) <= (others => DSP_DOUT(ac_width_out));
        
	-- Component Instantiations
	-- Slave AXI Stream Buffer for Data 
	S0_AXIS_FIFO_inst: S_AXIS_FIFO
		generic map(AXI_DATA_WIDTH => AXI_DATA_WIDTH,
					AXI_FIFO_DEPTH => S_AXI_FIFO_DEPTH)
		port map(clk => AXIS_ACLK,
				 ce => '1',
				 AXIS_RESETN => AXIS_RESETN,
				 S_AXIS_TDATA => S0_DATA,
				 S_AXIS_TVALID => S0_VAL,
				 S_AXIS_TREADY => S0_RDY,
				 S_AXIS_TLAST => S0_LAST,
				 S_AXIS_TKEEP => S0_KEEP,
				 LOAD_DONE => S_LOAD_DONE,
				 LOAD_EN => LOAD_EMPT_EN,
				 PROC_DONE => S_PROC_DONE,
				 PROC_EN => PROC_EN,
				 DSP_DATA_OUT => DSP_DIN,
				 DSP_VAL_OUT => DSP_VAL_IN,
				 LAST_OUT => DSP_LAST_IN);
				 
	-- Slave AXI Stream Buffer for Config 
	S1_AXIS_FIFO_inst: S_AXIS_FIFO
		generic map(AXI_DATA_WIDTH => AXI_DATA_WIDTH,
					AXI_FIFO_DEPTH => S_AXI_FIFO_DEPTH)
		port map(clk => AXIS_ACLK,
				 ce => '1',
				 AXIS_RESETN => AXIS_RESETN,
				 S_AXIS_TDATA => S1_DATA,
				 S_AXIS_TVALID => S1_VAL,
				 S_AXIS_TREADY => S1_RDY,
				 S_AXIS_TLAST => S1_LAST,
				 S_AXIS_TKEEP => S1_KEEP,
				 LOAD_DONE => S1_LOAD_DONE,
				 LOAD_EN => S1_LOAD_EMPT_EN,
				 PROC_DONE => S1_PROC_DONE,-- redo these signals brah
				 PROC_EN => S1_PROC_EN,
				 DSP_DATA_OUT => CONF_DIN,
				 DSP_VAL_OUT => CONF_VAL_IN,
				 LAST_OUT => CONF_LAST_IN);			 
				 
	-- Pipelined DSP FSM Instantiation 
	PIPE_DSP_FSM_inst: PIPE_DSP_FSM
		generic map(module_delay => MODULE_DELAY,
					counter_width => FSM_COUNT_WIDTH)
		port map(clk => AXIS_ACLK,
				 rst => not(AXIS_RESETN),
				 ce => '1',
				 en => '1',
				 val_in => DSP_VAL_IN,
				 last_in => DSP_LAST_IN,
				 DSP_en => DSP_EN,
				 val_out => DSP_VAL_OUT,
				 last_out => DSP_LAST_OUT,
				 state_out => open);
				 
	-- INSTANTIATE DSP MODULE HERE:
	-- Enabled Adder module 
	AC_Module_Inst: AC_complex_en
		port map(clk => AXIS_ACLK,
				 rst => not(AXIS_RESETN),
				 ce => '1',
				 data_I_in => DSP_DIN(2*IQ_dwid-1 downto 1*IQ_dwid),
				 data_Q_in => DSP_DIN(1*IQ_dwid-1 downto 0*IQ_dwid),
				 data_en => DSP_EN,
				 filt_I_in => CONF_DIN(2*IQ_dwid-1 downto 1*IQ_dwid),
				 filt_Q_in => CONF_DIN(1*IQ_dwid-1 downto 0*IQ_dwid),
				 filt_en => CONF_VAL_IN,
				 data_I_out => DSP_DOUT((ac_width_out+1+IQ_dwid)-1 downto 1*IQ_dwid),
				 data_Q_out => DSP_DOUT(ac_width_out downto 0*IQ_dwid));
	
	-- Master AXI Stream Buffer	
	M_AXIS_FIFO_inst: M_AXIS_FIFO
		generic map(AXI_DATA_WIDTH => AXI_DATA_WIDTH,
					AXI_FIFO_DEPTH => M_AXI_FIFO_DEPTH)
		port map(clk => AXIS_ACLK,
				 ce => '1',
				 AXIS_RESETN => AXIS_RESETN,
				 M_AXIS_TDATA => M_DATA,
				 M_AXIS_TVALID => M_VAL,
				 M_AXIS_TREADY => M_RDY,
				 M_AXIS_TLAST => M_LAST,
				 M_AXIS_TKEEP => M_KEEP,
				 EMPTY_DONE => M_EMPT_DONE,
				 EMPTY_EN => LOAD_EMPT_EN,
				 PROC_DONE => M_PROC_DONE,
				 PROC_EN => PROC_EN,
				 DSP_DATA_IN => DSP_DOUT,
				 DSP_VAL_IN => DSP_VAL_OUT,
				 LAST_IN => DSP_LAST_OUT);
	
	-- Load/Empty and Process control FSM for data
	AXIS_CYCLE_FSM_inst: AXIS_CYCLE_FSM
		port map(clk => AXIS_ACLK,
				 rst => not(AXIS_RESETN),
				 ce => '1',
				 S_LOAD_DONE => S_LOAD_DONE,
				 S_PROC_DONE => S_PROC_DONE,
				 M_PROC_DONE => M_PROC_DONE,
				 M_EMPT_DONE => M_EMPT_DONE,
				 LOAD_EMPT_EN => LOAD_EMPT_EN,
				 PROC_EN => PROC_EN,
				 state_out => open);
				 
	-- Load/Empty and Process control FSM for config
	AXIS_CYCLE_FSM_Config_inst: AXIS_CYCLE_FSM
		port map(clk => AXIS_ACLK,
				 rst => not(AXIS_RESETN),
				 ce => '1',
				 S_LOAD_DONE => S1_LOAD_DONE,
				 S_PROC_DONE => S1_PROC_DONE,
				 M_PROC_DONE => '1',
				 M_EMPT_DONE => '1',
				 LOAD_EMPT_EN => S1_LOAD_EMPT_EN,
				 PROC_EN => S1_PROC_EN,
				 state_out => open);			 
	
end behavior;  